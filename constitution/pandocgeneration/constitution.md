---
title: "Balliol College Junior Common Room"
author: ""
date: ""
---

# PRELIMINARY

## Definitions

- Academic year means the period from the beginning of Michaelmas Term until the end of Trinity Term, as reckoned according to the dates fixed by the Council of the University and published in the Oxford University Gazette;

- Annual General Meeting means the General Meeting held in 8th week of Michaelmas Term, as defined in part VII, subsection 1.2;

- Associate Member has the meaning given by part III, section 2;

- Battels means the billing method used by Balliol College to bill students for services and other charges; the process of being charged battels is called batteling.

- Chair refers to the individual responsible for presiding at meetings of a Committee or Supporting Committee or General Meeting.

- College means Balliol College, Oxford, which is a registered charity in the UK and Wales (No. 1144032) and an establishment to which part II of the Education Act 1994 applies;

- Committee means the committee established by part IV, section 1, who comprises the Committee Officers of the JCR;

- Committee Meeting means a meeting of the Committee, as defined by part V, section 4;

- Committee Office means a specific position that renders its holder(s) subject to certain dutiesunder part V;

- Committee Officer means a member holding a Committee Office.

- Dual nomination means the procedure by which two individuals may jointly stand for election or co-option for an Office;

- Emergency General Meeting means a time-sensitive General Meeting, as defined in part VII, subsection 1.3;

- Full Term means the period of Michaelmas Term, Hilary Term or Trinity Term, as reckoned according to the dates fixed by the Council of the University and published in the Oxford University Gazette;

- General Meeting mandate means a motion passed by a simple majority at a General meeting of the JCR;

- General Meeting means a meeting to which all Members are invited and maybe either the Annual General Meeting, an Emergency General Meeting or an Ordinary General Meeting;

- JCR Financial Year means the period beginning on 1 December each year and ending on 31 November the following year;

- JCR means the unincorporated association that is Balliol JCR;

- JCR Office Manager means the person employed on a permanent basis by the JCR to manage the records and accounts. They are not a member of the JCR;

- Master means the College officer responsible for the governance of the College, as defined in section II of the Statutes of the College;

- MCR means the Balliol College Middle Common Room;

- Member means an Ordinary Member or Associate Member of the JCR;

- Non-Committee Office means a position that confers on its holder(s) certain duties under part VI;

- Non-Committee Officer means an individual elected or co-opted to a Non Committee Office;

- Norway Room means the premises provided by the College for use by the JCR as a common room;

- Objects means the objects of the JCR set out in part II, section 1;

- Office means a Committee Office or a Non-Committee Office;

- Officer means an individual that holds a Committee Office or a Non-Committee Office;

- Ordinary General Meeting means a General Meeting other than an Annual General Meeting or Emergency General Meeting;

- Ordinary Member has the meaning given by part III, section 1;

- Pantry means the cafe situated in the Norway Room of Balliol College;

- President means the Committee Office that presides over the Committee;

- Secretary means the Committee Office responsible for recording JCR business;

- Standing Orders mean the rules and byelaws adopted by resolution of the Members that govern the JCR, subject to the provisions of this Constitution;

- Student means a matriculated student of the University;

- Supporting Committee means any body as described in part VI that supports the Committee in the execution of its duties, or otherwise acts as an advisory body to the Committee;

- Suspended Status Student means an undergraduate student who has been temporarily granted suspension of their studies by the College's Academic Progress Committee;

- The Lindsay Bar means the complex of rooms, including bar, pool room and beer cellar situated under the Norway Room of Balliol College;

- Treasurer means the Committee Office responsible for overseeing the finances of the JCR.

- University means the University of Oxford

## Interpretation

- Where this Constitution fixes a period of time by reference to a day or an event, that period of time does not include that day or the day of that event.

- Where this Constitution refers to the giving of notice in writing, writing includes communication by email.

- Where this Constitution refers to legislation, those references are references to such applicable legislation, including amendments, as may be in force from time to time.

- Where this Constitution refers to a ‘Connected Person’, ‘Connected Person’ means:

	- A child, parent, grandchild, grandparent, brother or sister of the Committee Officer;

	- The spouse or civil partner of the Committee Officer or of any person falling within sub-clause (a) above;

	- A person carrying on business in partnership with the Committee Officer or with any person falling within sub-clause (a) or (b) above;

	- An institution which is controlled:

		- By the Committee Officer or any Connected Person falling within subclause (a), (b) or (c) above;

		- By two or more persons falling within sub-clause (d)(i), when taken together.

	- A body corporate, in which

		- The Committee Officer or any connected person falling within subclauses (a) to (c) has a substantial interest; or

		- Two or more persons falling within sub-clause (e)(i) who, when taken together, have a substantial interest.

- Where this Constitution refers to ‘women’, ‘women’ shall be interpreted as including individuals wholly or partially self-identifying as women.

- Should any question arise over the interpretation of a General Meeting mandate, or of this Constitution the JCR Committee shall issue a binding interpretation, which shall be minuted and publicised by the Secretary.

- A petition containing 20 signatures of Ordinary Members objecting to the Committee’s interpretation of a General Meeting mandate and proposing an alternative interpretation, received no later than seven days after the interpretation has been publicised, shall be sufficient to trigger an Emergency General Meeting on the subject of the interpretation of the General Meeting mandate in question.

## Notice

- Subject to this Constitution, where a provision of this Constitution requires that notice of any matter be given to Members, that notice shall be given in writing:

	- Circulated to members via email; and

	- Circulated by means sufficient to comply with part IV, section 8.

- Failure to give notice in accordance with clause 3.1 does not affect the validity of anything done, or purportedly done, in accordance with this Constitution, if the person required to give notice used the person’s best efforts, in good faith, to give effective notice of the matter to Members.

# THE JCR

## Name and Objects

- The name of the unincorporated association is ‘Balliol JCR’.

- The principal office of the JCR is in Balliol College, Broad Street, Oxford, Oxfordshire, England OX1 3BJ.

- The Objects of the JCR are to advance education and to advance citizenship and community development by:

	- promoting the interests of Members and representing their opinions as members of the College and the University; and

	- acting as an intermediary between Members and the College in order to facilitate the fulfilment of the Objects of the JCR; and

	- supporting the educational objectives of the College; and

	- promoting facilities and services for Members’:

		- education at the University and the College; and

		- recreational and leisure-time activities and, in particular, their personal and cultural development;

	- encouraging, facilitating and providing opportunities for Members’ full participation in charitable activities, and in the management of charitable activities; and

	- promoting access to the College and the University by outreach to schools and the wider community; and

	- providing financial support to dramatic productions involving Members.

## Powers

- Subject to this Constitution, the JCR has all the powers that:

	- may be exercised by or on behalf of an unincorporated association by law; and

	- may be exercised to further its Objects or are conducive or incidental to doing so

- Without limiting the generality of clause 2.1, the JCR has power to:

	- raise moneys by such means as the Committee may from time to time determine; and

	- accept any gift or donation that will further the promotion of the Objects;

	- require payment by Members for use of any specified facility owned by the JCR or for participation in any of the JCR’s activities; and

	- employ such Ordinary Members as the Committee may decide is necessary to aid the running of the JCR, for example in the Lindsay and Pantry.

- The JCR does not have the power to permanently employ staff to aid the management of the JCR.

- The JCR shall:

	- be a non-discriminatory body in respect of race, sex, sexuality, gender identity, disability, nationality or religious orientation; and

	- operate in a fair and democratic manner; and

	- be accountable for its finances; and

	- treat the Norway Room with care and protect it, and all other property of the College used in connection with JCR activities, from damage.

## Application of income and property

- The income and property of the JCR shall be applied solely towards the promotion of its Objects.

	- a Committee Officer is entitled to be reimbursed from the property of the JCR or may pay out of such property reasonable expenses properly incurred by said Committee Officer when acting on behalf of the JCR.

- None of the income or property of the JCR may be paid or transferred directly or indirectly by way of dividend, bonus or otherwise by way of profit to any member of the JCR. This does not prevent a member receiving:

	- a benefit from the JCR as a beneficiary of the JCR;

	- reasonable and proper remuneration for any goods or services supplied to the JCR.

- Nothing in this clause shall prevent a Committee Officer or Connected Person receiving any benefit or payment which is authorised by part V, subclause 3.1 c) (iii).

# MEMBERSHIP

## Ordinary Members

- A person is an Ordinary Member if the person:

	- is eligible to be an automatic Ordinary Member under clause 1.2; and

	- would not automatically cease to be a Member under section 5, and has not exercised the person’s right not to be an Associate or Ordinary Member, in accordance with section 4, or is an MCR member who has indicated, in writing addressed to the President, that the person wishes to be a Member.

- A person is eligible to be an automatic Ordinary Member if the person

	- is a natural person; and

	- is an undergraduate student:

		- is currently a member of the College; or

		- is a Suspended Status Student.

- An Ordinary Member has the right to:

	- use all JCR service and facilities; and

	- attend, speak or vote at JCR meetings; and

	- vote in JCR elections; and

	- stand in JCR elections, unless they are a Suspended Status Student.

- An Ordinary Member is obligated to pay all JCR compulsory levies, unless they are a Suspended Status Student.

## Associate Members

- A person is an Associate Member if the person:

	- is eligible to be an automatic Ordinary Member under clause 1.2; and

	- would not automatically cease to be a Member under section 5, and

	- has exercised the person’s right not to be an Ordinary Member, but has not exercised the right not to be an Associate Member in accordance with section 4.

- An Associate Member shall have all the rights of an Ordinary Member, except:

	- the right to attend, speak or vote at JCR meetings; and

	- the right to stand or vote in JCR elections; and

	- the obligation to pay JCR compulsory levies of a political or charitable nature.

- An Associate Member is subject to any disciplinary measures issued by the Master or the Master’s designee and shall comply with them.

## Honorary Members

- A person is an Honorary Member if the person:

	- is not eligible to be an automatic Ordinary Member under clause 1.2; and

	- has not exercised either the right not to be an Ordinary Member, or the right not to be an Associate Member in accordance with section 4.

	- has been declared an Honorary Member by means of a motion passed at a General Meeting.

- An Honorary Member has no rights, obligations or liabilities in connection with the JCR, and shall be considered a member of the JCR in name only and not in fact.

## Right not to be a Member

- A person who is eligible to be a Member has the right to be either an Ordinary Member or Associate Member, or to opt-out of Membership entirely.

- The President shall, no later than Sunday of the First Week of Michaelmas Term each year, give notice to Members of the rights provided in this section.

- A person may exercise the rights provided in this section:

	- at any time; and

	- no more than once per Academic Year; and

	- by indicating in writing, addressed to the President, that the person wishes to either become an Associate Member or opt-out of membership entirely.

- A person who has exercised the rights provided in this section may revoke the exercise of that right and become a Member

	- during the first two weeks of any Full Term; and

	- no more than once per Academic Year; and

	- by indicating in writing, addressed to President, that the person revokes the person’s previous indication under subsection 4.3 and now wishes to be a Member.

- A person who has exercised the right provided in this section to opt out of Membership entirely is no longer required to pay compulsory levies, but in addition to the losses specified in subsection 2.2 such a person shall be denied:

	- service at the JCR pantry; and

	- service at the Lindsay Bar; and

	- use of the washing machines.

## Termination of Membership

- Membership of the JCR comes to an end if:

	- the Member dies; or

	- the Member exercises their right not to be a Member under section 4; or

	- any sum of money owed by the Member to the JCR is not paid in full within six months of its falling due.

## Liabilities

- If the JCR is wound up in accordance with part IV section 7, the Members have no liability to contribute to its assets and no personal responsibility for settling its debts or liabilities.

# ADMINISTRATION OF THE JCR

## Administrative Bodies

- The general administration and management of the JCR and its affairs is governed by the Committee, as detailed in part V.

- The Committee is supported and assisted in its duties by multiple Supporting Committees and Non-Committee Offices, as detailed in part VI.

- All committees described herein may only convene between 0th week and 9th week, inclusive, of Oxford University Academic terms.

	- ‘Convene’ does not include discussions on the JCR Committee Facebook page, emails, or other means of communication between Committee members, if the discussion is for the regular policy and continuing functions of the JCR.

	- If the topic of discussion is beyond the regular policy and continuing functions of the JCR, no action is to be taken until 0th week of the next term, where Committee may convene in person.

	- The decision as to whether a topic of discussion is extra-ordinary to regular policy and continuing functions of the JCR rests, in the first instance, with the President.

	- If the President decides a topic of discussion to be extra-ordinary, it may be a time- sensitive issue. In this case:

		- The decision to designate the issue as time-sensitive rests, in the first instance, with the President. This is the only exception to (b).

		- The nature and execution of a subsequent course of action rests, in the first instance, with the President.

	- Any member of the Committee who believes it to be appropriate may challenge the President’s decision in (c), or either part of (d), by calling a vote on the matter (a vote via Facebook poll is acceptable), and the President’s decision may be overruled by a Committee majority of 2/3.

	- In all cases where a decision is made by the President as detailed in (c) and (d), a rationale is to be supplied.

	- For clarity: the situation runs as follows: an issue arises, and discussion occurs. The President may halt the discussion by deeming it extra-ordinary. The President may also deem it time-sensitive and determine a course of action. Any of the decisions may be overruled by a Committee majority of 2/3.

- Elections and co-options for Committee Offices and Non-Committee Offices take place under the procedure specified in part VIII.

- Any resolution of a General Meeting, as specified in part VII, shall be binding upon all Officers, the Committee and all Supporting Committees, and all Ordinary and Associate Members, provided the resolution is consistent with the law, this Constitution and Standing Orders, and is not frivolous, vexatious or defamatory.

- The JCR shall comply with its obligations in relation to the keeping of, and provision of access to, registers of its Members and Committee Officers.

## Governing Documents

- Amendment of the Constitution

	- An amendment to this Constitution may only be adopted by resolution passed by a 75% majority of votes cast on two readings of the proposed amendment, to take place at two consecutive General Meetings called in accordance with part VII of this Constitution.

	- Any amendment to the Constitution shall be submitted to the executive committee of the College at its next meeting after the date of the General Meeting which passed the amendment.

	- Should a resolution proposing a change to the Constitution be amended at its second reading, it shall be regarded as a new resolution to alter the Constitution undergoing its first reading under subsection 2.1a). 2.2. Standing Orders

	- Standing Orders may define Non-Committee Offices and their duties, impose additional duties upon Members and Committee Offices, define the Supporting Committees, and govern procedural matters.

	- Standing Orders are binding upon the Committee Officers, the Committee, all Supporting Committees and Non-Committee Offices, and all Members.

	- Standing Orders may be amended by a resolution passed by a 75% majority of votes cast at a General Meeting called in accordance with part VII of this Constitution.

	- No amendment that is inconsistent with the provisions of this Constitution shall be valid.

	- In the event that a provision in the Standing Orders conflicts with any provision of this Constitution, the Constitution shall control.

## Records and Accounts

- The Committee Officers shall comply with the requirements of the Constitution and Standing Orders with regard to the keeping of account records, to the preparation and scrutiny of statements of accounts, and to the preparation of annual reports and returns.

## Execution of Documents

- The JCR shall execute legally binding documents by signature.

- A document is validly executed by signature if it is signed by at least three Committee Offices including either the President, Vice-President (Administrative) or Treasurer.

## JCR Accounts and Finance

- The JCR shall maintain such operational and capital accounts as are necessary and convenient for the management of the JCR’s finances.

- The Committee, and in particular the Treasurer, shall maintain the JCR’s financial records in a manner that:

	- correctly records and explains the financial transactions and financial position of the JCR; and

	- enables true and fair accounts of the JCR to be prepared and examined from time to time.

- Without limiting the generality of clause 5.2, the JCR’s financial records shall contain:

	- entries showing from day to day all sums of money received and expended by the JCR, and the matters in respect of which the receipt and expenditure took place; and

	- a record of the assets and liabilities of the JCR.

- The Treasurer shall:

	- ensure that the JCR’s financial records are preserved for at least six years from the end of the JCR Financial Year to which they relate; and

	- prepare and submit, in Hilary Term of each year, a budget for the JCR for approval by the executive committee of the College; and

	- ensure that all expenses incurred by Committee Officers and Non-Committee Officers from budgets or accounts assigned under clause 5.6 are authorised; and

	- be responsible for the nature and fulfilment of any payments or loans made by the JCR or to the JCR; and

	- at the request of a Member, make available a copy of the JCR’s financial records to said Member.

- All payments of authority and cheques from JCR accounts must be signed by:

	- The JCR Office Manager; and

	- If from an account designated for management of the Lindsay Bar, the Treasurer or the Committee Office responsible for the management of the Lindsay Bar; or

	- If from any other JCR account, one of either the President or the Treasurer.

- Committee Offices, Supporting Committees or Non-Committee Offices may have termly budgets or individual accounts allocated to them, as detailed in this Constitution and Standing Orders. These may be spent:

	- at the discretion of the relevant individuals; and

	- provided that the funds are used for a purpose directly related to said Committee Office, Supporting Committee or Non-Committee Office; and

	- if the budget or account belongs to a Non-Committee Office, with the consent of the Treasurer.
- In the event that Committee Offices, Supporting Committees or Non-Committee Offices incur necessary expenditures and do not have a budget (or are unable to match the expenditure with their budget), additional funds equal to the shortfall may be passed by the following means, subject to the constraints of the law and this Constitution:

	- beforehand by the General Meeting if the shortfall is over £150; or

	- by the General Meeting if the shortfall is £50 or more, but less than £150; or

	- by the Committee if the shortfall is under £50.

## Referenda

- A referendum may be called by:

	- The Committee, where the President will draft the question to be put.

	- A General Meeting, by passing a motion which resolves to have a referendum, and contains within it the question to be put.

- At least five days’ notice of a referendum, and the question to be put, shall be given to the JCR.

- Procedure for Referenda will be the same as the procedure for elections laid out in part VIII, with the following changes:

	- The Returning Officer shall be selected by the Committee, and shall be a person judged to be neutral on the question being put.

	- The only publicity that may be produced is a document of not more than 500 words to be displayed in the JCR. Any group may request to produce such a document, and the Returning Officer may grant several such requests, with several documents being displayed. All documents shall conform with the Constitution.

	- Any complaint relating to the conduct of a referendum shall be submitted to either the Returning Officer or the President within 24 hours of the referendum result being officially announced.

- No referendum may amend the Constitution.

- A referendum that amends Standing Orders requires a 75% majority to pass. All other referenda, unless specified otherwise in this Constitution or Standing Orders, require a simple majority to pass.

- The results of a referendum are binding, providing they are within the law and this Constitution, and not counter to the Objects of the JCR.

- Interpretation of the outcome of a referendum is the responsibility of the Committee, but the Committee shall make clear how they intend to interpret the question when it is submitted, if there is any ambiguity.

## Dissolution of the JCR

- The JCR may be dissolved by resolution of its members. Any decision by the members to wind up or dissolve the JCR can only be made:

	- at a general meeting of Members called in accordance with part VII of this Constitution;

	- by a resolution passed by decision taken without a vote and without any expression of dissent in response to the question put to the general meeting; or

	- by a resolution agreed in writing by all Members.

- Subject to the payment of all the JCR’s debts:

	- Any resolution for the winding up of the JCR, or for the dissolution of the JCR without winding up, may contain a provision directing how any remaining assets of the JCR shall be applied.

	- If the resolution does not contain such a provision, the Committee Officers shall decide how any remaining assets of the JCR shall be applied.

	- In either case the remaining assets shall be applied for charitable purposes the same as or similar to those of the JCR.

## Use of electronic communications

- The JCR will provide any Member on request with a hard copy of any document or information sent to the member other than in hard copy form, within 21 days.

- Any Member or Committee Officer may communicate electronically with the JCR to an address specified by the JCR for the purpose, so long as the communication is authenticated in a manner which is satisfactory to the JCR.

- Any Member or Committee Officer of the JCR, by providing the JCR with their email address, is taken to have agreed to receive communications from the JCR in electronic form at that address, unless the Member has indicated their unwillingness to receive such communications in that form.

- The Committee Officers may, subject to compliance with any legal requirements, by means of publication on its website:

	- provide Members with notice under part I, section 3; and

	- provide Members with any other relevant information.

- The Committee Officers shall:

	- take reasonable steps to ensure that Members and Committee Officers are promptly notified of the publication on the website of any notices or other information; and

	- send any such notice or other information in hard copy form to any Member or Committee Officer who has not consented to receive communications in electronic form.

## Dispute Resolution

- If a dispute arises between Members about the validity or propriety of anything done by the Members under this constitution, and the dispute cannot be resolved by agreement, the parties to the dispute must first try in good faith to settle the dispute by mediation before resorting to litigation.

# THE COMMITTEE

## Powers of the Committee

- Subject to this Constitution, the Committee has all the functions and powers necessary or convenient for the management and administration of the JCR and its affairs.

- In exercising its functions and powers, the Committee shall have regard to the resolutions of General Meetings.

- Between General Meetings, the Committee may create ad hoc Committees or temporary posts as it sees fit, subject to the approval of the next General Meeting.

- The Committee may create working groups to investigate a specified issue and submit a report to the Committee on said issue. These working groups have no powers, plenipotentiary or otherwise, and are automatically dissolved when their task is complete.

## Committee Offices

- The Committee is composed of multiple Committee Offices.

- The individuals who hold Committee Offices are appointed by election in accordance with part VIII.

- At the final Committee Meeting of Michaelmas Term, all Committee Officers shall resign their positions and the Committee-Elect shall assume their duties.

- The following positions are Committee Offices, and may be held only by a single individual:

	- The President;

	- The Vice-President (Administrative);

	- The Vice-President (Organisational)

	- The Treasurer;

	- The Secretary;

	- The Lindsay;

- The following positions are Committee Offices, and may be held either by a single individual or by dual nomination:

	- The Academic Affairs and Careers Officer;

	- The Access and Admissions Officer;

	- The Affiliations Officer;

	- The Amalgamated Sports Rep;

	- The Charities and RAG Officer;

	- The Class Affiliations and First Generation Officer;

	- The Design and Maintenance Officer;

	- The Student Disabilities Officer;

	- The Entz Officer;

	- The Environment and Ethics Officer;

	- The Ethnic Minorities Officer;

	- The Foody;

	- The Housing Officer;

	- The International Students’ Officer;

	- The John de Balliol;

	- The LGBTQ Officer;

	- The Dr. WHO (Well-being and Health Officer);

	- The Women’s Officer;

	- The Website and Computing Officer;

	- The Suspended Students’ Officer.

- Additional duties may be imposed upon specific Committee Offices by this Constitution, by Standing Orders or by resolutions of General Meetings.

- No individual may hold two Committee Offices concurrently, unless temporarily exercising the duties of other Offices under subsection 5.5.

- Committee Offices specified in subsection 2.5 may be jointly held by two individuals, provided that:

	- Both individuals are eligible as Committee Officers in their own right under subsection 3.3.

	- Both individuals are elected to that position as a dual nomination, unless one individual has left or been removed from office under subsection 3.5 and subsequently replaced under section 5 or the Committee has agreed that a second officer can be added, and they are appointed under section 5.

- If a Committee Office is jointly held, the Committee Office is counted as present if at least one Committee Officer who holds the Committee Office is present.

- If two Members jointly hold a Committee Office, both are ex officio Committee Officers of the JCR.

## Committee Officers

- Duties and Responsibilities of Committee Officers

	- Committee Officers shall:

		- exercise their powers to perform their functions as a Committee Officer of the JCR in a way they decide in good faith would be most likely to further the Objects of the JCR; and

		- exercise, in the performance of those functions, such care and skill as is reasonable in the circumstances, having regard in particular to:

            1. any special knowledge or experience that they have or hold themselves out as having; and

            2. if they act as a Committee Officer of the JCR in the course of business or profession, any special knowledge or experience that it is reasonable to expect of a person acting in the course of that kind of business or profession.

	- No Committee Officer may make comments in their representative capacity which the JCR considers to be adversely discriminating in respect to race, sex, sexuality, disability, nationality or religious orientation.

	- No Committee Officer or Connected Person may:

		- buy or receive any goods or services from the JCR on terms preferential to those for members of the public; or

		- sell goods, services, or any interest in land to the JCR; or

		- be employed by, or receive any remuneration from, the JCR, unless they are employed in their capacity as an Ordinary Member. The conditions of such employment shall be approved by the Committee.

	- In the event that a Committee Officer holds any interest in a proposed transaction or arrangement with the JCR, be it direct or indirect, they shall:

		- declare the nature and extent of that interest, unless previously declared; and

		- absent themselves from any discussions of the committee in which it is possible that a conflict of interest will arise between their duty to act solely in the interests of the JCR and any personal interest (including but not limited to any financial interest). It shall be noted that:

            1. Any Committee Officer absenting themselves under this clause shall not vote or be counted for quorum in any decision on this matter by the Committee; and

            2. The Secretary shall record every disclosure made in accordance with this subsection in the minutes of the Committee Meeting at which it is made.

	- Unless they have a reasonable excuse for not attending and have given notice in writing of that excuse to the Secretary in advance, all Committee Offices shall attend:

		- all regularly scheduled meetings of the Committee; and

		- all Supporting Committees of which they are a member.

	- All Committee Offices shall, no later than Sunday of the Eleventh Week of each term, submit to the Secretary a report detailing their activities for that term. These reports shall be compiled by the Secretary, who shall:

		- distribute the reports to the JCR before Sunday of 0th week of the following term; and

		- ensure that they are preserved at least six years after their creation.

	- Additionally, all Committee Offices shall, no later than Sunday of Eighth Week of Michaelmas term, submit to their successor a handover pack detailing specific instructions and containing materials related to their position.

	- All duties outlined in this section represent minimum requirements for Committee Offices. All Committee Offices may exceed these duties in any way that furthers the Objects of the JCR, providing it is not contrary to this Constitution.

- Powers of Committee Officers

	- The Committee Officers have all the powers necessary or convenient to assist the Committee in managing the JCR’s affairs within their areas of responsibility.

- Eligibility to be a Committee Officer

	- Subject to the restrictions in this clause, all Ordinary Members who are not Suspended Status Students or who are on academic probation shall have the right to stand for any Committee Office.

	- Every Committee Officer shall be a natural person.

	- No one may be elected to a Committee Office:

		- if they are under the age of 16 years; or

		- if they would automatically cease to hold office under the provisions of subsection 3.5.

	- No-one who is not an Ordinary Member may stand for election or act as a Committee Officer.

	- No one may stand for two Committee Offices at the same election or stand for a Committee Office to be held concurrently with a Committee Office already held, except under subsection 5.5.

	- No one may act as a Committee Officer, whether on appointment or on any re- appointment, until they have expressly acknowledged, in the manner specified by the Committee, their acceptance of their Committee Office.

	- Only individuals who self-identify partially or wholly as a woman may hold the positions of Women’s Officer.

	- Only individuals who are disabled, international, from an ethnic minority background, or who identify as LGBTQ should hold the positions of, Student Disability Officer, International Students’ Officer, Ethnic Minorities Officer, or LGBTQ Officer, respectively.

	- If the position of Amalgamated Sports Rep is held by two individuals, they must both self-identify as different genders.

- Number of Committee Officers

	- There shall be at least eight Committee Officers. If the number falls below this minimum, the remaining Committee Officer or Members may act only to call a meeting of the Committee, or to co-opt a new Committee Officer.

	- There is no maximum number of Committee Officers or Committee Offices that may be appointed to the JCR.

- Removal of Committee Officers

	- A Committee Officer ceases to hold office if they:

		- resign from their office by notifying the JCR and Committee in writing;

		- are absent from all meetings for a period of six months;

		- die;

		- become incapable by reason of mental disorder, illness or injury of managing and administering their own affairs;

		- are removed by a vote of no confidence in accordance with subsection 3.6;

		- cease to be eligible as a Committee Officer under subsection 3.3.

- Motions of No Confidence

	- A motion of no confidence in a Committee Officer may be submitted by Ordinary Members, and shall be considered valid if signed by at least 20 Ordinary Members.

	- A motion of no confidence shall specify the exact actions of the Committee Officerthat contravene this Constitution, whether that be dereliction of any duties set forth in this Constitution and the Standing Orders, or other illegal activity.

	- The opinion of the Committee on a pending motion of no confidence shall be decided by a recorded vote in Committee, which shall take place in camera and where directly involved Committee Officers have absented themselves under subclause 3.1.d)(ii).

	- If the Committee have supported the motion under clause 3.6 c), the Committee Officer in question shall be temporarily suspended from office until such time as the motion of no confidence has been voted on at the General Meeting called under clause e) of this subsection.

	- The motion shall be debated at an Emergency General Meeting called in accordance with part VII section 2, and shall pass if:

		- the Emergency General Meeting is convened and quorate in accordance with part VII section 7 of this Constitution; and

		- the Committee Officer in question is given reasonable opportunity to defend their actions, orally or in writing, at the Emergency General Meeting; and either

		- the Committee have supported the motion under clause 3.6 c) and a simple majority of Ordinary Members present and eligible to vote at the Emergency General Meeting vote in favour of the motion; or

		- the Committee does not support the motion but a two-thirds majority of Ordinary Members present and eligible to vote at the Emergency General Meeting vote in favour of the motion.

		- Where two individuals share the responsibilities of a single position under clause 2.8, one may cease to hold the position under subsection 3.5 without affecting the position of the other, who shall continue to carry out the duties of that position alone, until such time that a new Committee Officer to fill the role is selected under section 5.

- The founding Committee Officers, and the first Committee Officers of the JCR, are:

	- Sian Dennett, the President, and the author of this Constitution;

	- Polly Palmer-Jones, the Vice-President (Administrative)

	- Pippa Wong Vice-President (Organisational)

	- Matthew Shipway, the Treasurer;

	- Klemens Okkels, the Secretary;

	- Hannah O’Connor, the Lindsay;

	- David Danin, the Academic Affairs and Careers Officer;

	- Izzie Alexandrou, the Access and Admissions Officer;

	- Fiónn McFadden, the Affiliations Officer;

	- Henry Ferrabee and Jessica Cullen, The Amalgamated Sports Reps;

	- Georgie Cutmore and Oskar Bishop, the Charities and RAG Officers;

	- Efan Owen and Connor Connolly, The Class Affiliations and First Generation Officers;

	- Colm Britchfield and James Kavanagh, the Design and Maintenance Officers;

	- William Wilson and Jack Ovens, the Disabled Students’ Officers;

	- Cameron Tweed, Isabel Coates and Maisie King, the Entz Officers;

	- Charlotte Maddinson and Scarlett Stokes, the Environment and Ethics Officers;

	- Lilia Kanu and Jaden Ruddock, the Ethnic Minorities and International Students’ officers;

	- Callum Webb, the Foody;

	- Vignesh Iyer and Sidhaarth Kumar, The Housing Officers;

	- Otilia Casuneanu and Helen Trenner, The International Students’ Officers;

	- George Roupas, the John de Balliol;

	- Charley Archer and Debbie Lemke, the LGBTQ Officers;

	- Samuel Glossop, Lea Moutault and Sophie Winter, the Drs. WHO;

	- Felicity Thomas and Marina Ristuccia, the Women’s Officers;

	- Tobias Bretschneider, the Website and Computing Officer.

## Committee Meetings

- The Committee shall meet at least four times each term.

- A Committee Meeting may be called by the President or at the request of any three Committee Offices.

- Committee Meetings may be attended by any Ordinary Members who wish to do so, though they may be required to leave under clause 4.7.e).

- At least twenty-four hours before a Committee Meeting, the Secretary shall give notice, by email, to the Committee Officers, setting out:

	- when and where the Committee meeting is to be held; and

	- particulars of the business to be transacted at the Committee Meeting and of the order in which that business is to be transacted.

- Quorum and Procedure at Committee Meetings

	- At a Committee Meeting:

		- No decision shall be taken unless a quorum is present at the time when the decision is taken. The quorum is eight Committee Officers, including one of the President, Vice-President (Administrative) or Treasurer, entitled to vote under subclause 4.5a)(iv).

		- Unless the Committee resolves otherwise, any person may be invited by the Committee to attend and address the meeting;

		- The President, or in the absence of the President, the Vice-President (Administrative) or other Committee Officer nominated by the President, shall Chair the meeting;

		- Only Committee Officers who are present in person may vote;

		- Each Committee Officer entitled to vote under subclause 4.5a)(iv) has a deliberative vote and, if the votes are equal, the person Chairing the meeting shall exercise a casting vote;

		- A resolution is decided by a simple majority of votes cast by Committee Officers entitled to vote under subclause 4.5a)(iv) on a show of hands;

		- Subject to this Constitution, the meeting shall be conducted in the manner that the person Chairing the meeting decides.

- Saving Provisions

	- Subject to clauses (b) and (c) of this subsection, all decisions of the Committee Officers or of a committee of Committee Officers, shall be valid notwithstanding the participation in any vote of a Committee Officer:

		- who was disqualified from holding office; or

		- who had previously retired or who had been obliged by the Constitution to vacate office;

		- who was not entitled to vote on the matter, whether by reason of a conflict of interest or otherwise

	- Clause (a) of this subsection is only applicable if, without the vote of that Committee Officer and that Committee Officer being counted in the quorum, the decision has been made by a majority of Committee Officers at a quorate meeting.

	- Clause (a) of this subsection does not permit a Committee Officer to keep any benefit that may be conferred upon him or her by a resolution of the Committee Officers or of a committee of Committee Officers if, but for clause (a), the resolution would have been void, or if the Committee Officer has not complied with clause 3.1 (d).

- Records of Committee Meetings

	- The Secretary, or in the absence of the Secretary another Committee Officer chosen by the person Chairing the meeting, shall take full and accurate minutes of the proceedings of every Committee, including but not limited to:

		- the names of the Committee Officers present at the meeting; and

		- any co-option of Offices made by the Committee; and

		- discussions that take place at the meeting, excluding any discussions that take place in camera; and

		- decisions made at the meeting, excluding any decisions made in camera.

	- Minutes signed in accordance with clause 4.7a) are, unless the contrary is proved, evidence that:

		- the Committee Meeting to which they relate was duly convened and held;

		- all proceedings recorded as having taken place at the Committee meeting did in fact take place;

		- all co-options recorded as having been conducted at the Committee Meeting were validly conducted and the result of those co-options was recorded.

	- As soon as reasonably practicable after a Committee Meeting, the Secretary shall circulate, by email to the Committee Officers, a copy of the minutes taken in accordance with clause 4.7a).

	- The Secretary shall make a copy of the minutes taken in accordance with clause 4.7a) available to Members on request;

	- Subject to majority approval, the Committee may proceed in camera if the welfare or behaviour of a specific JCR member is being discussed.

		- Should the Committee proceed in camera, any Ordinary Members or other invited persons present shall be required to leave unless requested to stay by the majority of the Committee.

		- No minutes may be taken relating to discussions held in camera.

## Vacancies on Committee

- Should a Committee Office be left unfilled after the Committee-Elect take office in Michaelmas term, then it shall be filled by co-option by the procedure given in Part VIII.

- If, under subsection 3.5 a Committee Officer resigns or is removed from office, then the vacancy shall be filled by:

	- Election by secret ballot as laid out in Standing Orders, if there was already an election
to be held within four weeks of the Committee Officer leaving office; or

	- If a Committee Officer announces their intention to resign on a specified date, and there is an election to be held in the period between the Committee Officer’s announcement and resignation, then a new member may be elected to hold theirposition, who will take office upon the previous Committee Officer’s resignation.

	- Co-option, under any other circumstances.

- Committee Officers who fill a vacancy under the above two clauses shall hold office until the Annual General Meeting, unless they vacate the Office under subsection 3.5.

- Committee Offices may only be deemed to be vacant if no member holds the Office. If a Committee Office eligible for dual nomination is held by an individual, they may request that the Committee co-opt an additional member to hold the position with them.

- In the event of a temporary vacancy arising before an election can take place, the Committee Office may be assigned to another Committee Officer until such time as the vacancy is filled. This is the only means by which a Committee Officer may hold multiple Committee Offices at once.

- In the event that the entire Committee resigns, an interim President shall be elected at the General Meeting in which the Committee resign. The remaining Committee Offices shall then be filled by co-option until such a time as a full election may be held.

# SUPPORTING COMMITTEES AND NON-COMMITTEE OFFICE

## Supporting Committees

- The Committee Officers may define Supporting Committees, which are advisory bodies that exist to assist the Committee Officers in their duties. Any Supporting Committees created in this manner shall be defined in Standing Orders.

- The Committee Officers may delegate any of their powers or functions to the Supporting Committees as they see fit, and, if they do, they must determine the terms and conditions on which the delegation is made. The Committee Officers may at any time alter those terms and conditions, or revoke the delegation.

	- This power is in addition to any other power of delegation available to the Committee Officers, but is subject to the following requirements:

		- A Supporting Committee may consist of two or more persons, but at least one member of each Supporting Committee must be a Committee Officer; and

		- The acts and proceedings of any Supporting Committee must be brought to the attention of the Committee Officers as a whole as soon as is reasonably practicable; and

		- The Committee Officers shall from time to time review the arrangements which they have made for the delegation of their powers.

	- Supporting Committees may only contain Ordinary Members who are not Suspended Status Students.

	- Supporting Committees shall have only the powers necessary to fulfil their duties and functions.

	- Members who would not normally be permitted to attend a meeting of a Supporting Committee may be invited at the discretion of the Chair of the Supporting Committee.

## Non-Committee Offices

- The individuals who hold Non-Committee Offices are Non-Committee Officers.

- The Non-Committee Offices and their responsibilities are defined in Standing Orders.

- If specified in the definition of a particular Non-Committee Office, two individuals may jointly hold said Non-Committee Office, provided that:

	- Both individuals are eligible as Non-Committee Officers in their own right under subsection 3.3.

	- Both individuals are elected to that position as a dual nomination, unless one individual has left or been removed from office under subsection 3.4 and subsequently replaced under section 4.

	- 4 If a Non-Committee Office is jointly held, said Non-Committee Office is counted as present if at least one representative of the Non-Committee Office is present.

## Non-Committee Officers

- Duties of Non-Committee Officers

	- Non-Committee Officers shall perform any duties assigned to them by this Constitution, by Standing Orders and by resolutions of General Meetings, and as directed by the Committee Offices and Members, to the extent of their abilities.

	- Unless they have a reasonable excuse for not attending and have given notice in writing of that excuse to the Secretary in advance, all Non-Committee Offices shall be present at the meetings of any Supporting Committees of which they are a member.

- Powers of Non-Committee Officers

	- For the avoidance of doubt, Non-Committee Officers are not considered CommitteeOfficers of the unincorporated association that is Balliol College JCR.

	- The Non-Committee Officers have only the powers necessary for the fulfilment of their functions and duties.

	- Non-Committee Officers are elected to fulfil only the specific duties outlined in this Constitution and Standing Orders, and hold no wider power over the affairs of the JCR or plenipotentiary powers on behalf of the Committee.

	- All expenditure by Non-Committee Officers shall be first approved by the Committee, and are subject to the restrictions of part IV, clauses 5.6 and 5.7.

- Eligibility to be a Non-Committee Officer

	- Subject to the restrictions in this clause, all Ordinary Members who are not Suspended Status Students shall have the right to stand for any NonCommittee Office.

	- Every Non-Committee Officer shall be a natural person.

	- No one may be elected to a Non-Committee Office:

		- if they are under the age of 16 years; or

		- if they would automatically cease to hold office under the provisions of part V, subsection 3.5 mutatis mutandis.

	- Multiple Non-Committee Offices may be held concurrently, and Non- Committee Offices can be held concurrently with a Committee Office.

	- No one is entitled to act as a Non-Committee Officer whether on appointment or on any re-appointment until they have expressly acknowledged, in whatever way the Committee decides, their acceptance of the office of Non-Committee Officer.

	- The role of Duckworths will be carried out by two individuals who both self-identify as different genders.

- Removal of Non-Committee Officers

	- A Non-Committee Officer ceases to hold office if they:

		- resign from their office by notifying the JCR and Committee in writing;

		- are absent from all meetings for a period of six months;

		- die;

		- become incapable by reason of mental disorder, illness or injury of managing and administering their own affairs;

		- cease to be eligible as a Non-Committee Officer under subsection 3.3;

		- are removed by a vote of no confidence in accordance with subsection 3.5.

- Motions of No Confidence

	- A motion of no confidence in a Non-Committee Officer may be submitted by Ordinary Members, and shall be considered valid if signed by at least 20 Ordinary Members.

	- A motion of no confidence shall specify the exact actions of the NonCommittee Officer that contravene this Constitution, whether that be dereliction of any duties set forth in this Constitution and the Standing Orders, or other illegal activity.

	- The opinion of the Committee on a pending motion of no confidence shall be decided by a recorded vote in Committee, which shall take place in camera.

	- If the Committee have supported the motion under clause 3.6 c), the NonCommittee Officer in question shall be temporarily suspended from office until such time as the motion of no confidence has been voted on at the General Meeting called under clause e) of this subsection.

	- The motion shall be debated at an Emergency General Meeting called in accordance with part VII section 2, and shall pass if:

		- the Emergency General Meeting is convened and quorate in accordance with part VII section 7 of this Constitution; and

		- the Non-Committee Officer in question is given reasonable opportunity to defend their actions, orally or in writing, at the Emergency General Meeting; and either

		- the Committee have supported the motion under clause 3.6 c) and a simple majority of Ordinary Members present and eligible to vote at the Emergency General Meeting vote in favour of the motion; or

		- the Committee does not support the motion but a two-thirds majority of Ordinary Members present and eligible to vote at the Emergency General Meeting vote in favour of the motion.

	- Where two individuals share the responsibilities of a single position under clause 2.8, one may cease to hold the position under subsection 3.5 without affecting the position of the other, who shall continue to carry out the duties of that position alone, until such time that a new Non-Committee Officer to fill the role is selected under section 4 to Part VI of the constitution.

## Non-Committee Vacancies

- Non-Committee Offices for which no people are elected may, but are not required to be, filled by co-option as described in Part VIII.

- If, at any election, there are Non-Committee Offices vacant, they shall be open for nominations at that election.

- In the event of a Non-Committee Officer’s resignation or removal, it is at the Committee’s discretion whether to advertise the position for cooption or leave it vacant until the next planned election.

- Non-Committee Offices elected or co-opted to fill a vacancy shall hold office until the Annual General Meeting, unless they vacate the Office under subsection 3.5.

# GENERAL MEETINGS

## Types of General Meeting

- The three types of General Meeting are the Ordinary General Meeting, the Emergency General Meeting, and the Annual General Meeting.

- Annual General Meeting

	- The Annual General Meeting shall be held in 8th Week of Michaelmas Term every academic year.

	- The annual statement of audited accounts and the Committee’s annual report shall be presented to the Annual General Meeting.

	- All Standing Policy dated over three years prior to the Annual General Meeting is open for amendment or deletion, with a simple majority required for any changes.

	- By request of at least 5% of the Ordinary Members, the affiliation of the JCR to a particular organisation may be decided by secret ballot at the Annual General Meeting.

- Emergency General Meeting

	- Emergency General Meetings are for discussion of time sensitive issues, and have special provisions regarding their calling and what may be discussed there, outlined below.

- Ordinary General Meeting

	- Ordinary General Meetings are for all other business of the JCR.

	- Motions to make charitable donations from the JCR Charity Fund to a cause which would suffer if the motion were delayed until the online charities ballot, as arranged by the Charities & RAG Officer in accordance with part III Clause 14.1.d) of the Standing Orders, they may be brought to the next Ordinary General Meeting.

	- Motions to make charitable donations from the JCR General Fund which are distinct from donations resulting from the online charities ballot as arranged by the Charities & RAG Officer in accordance with part III Clause 14.1.d) of the Standing Orders may be brought to an Ordinary General Meeting by an Ordinary Member. Any charity receiving money from the JCR General Fund:

		- may receive no more than £200 at a time;

		- may only receive one donation per term;

		- must be a registered charity in England and Scotland and must have a clear connection with the welfare of students at Balliol;

		- no more than £1000 can be donated in any given term and no amount of this money can roll over to a following term.

		- A charity cannot both receive money from the JCR Charity Fund and JCR General Fund.

	- There shall be at least two Ordinary General Meetings per academic term.

## Calling General Meetings

- The Committee is responsible for calling General Meetings, and shall call all General Meetings with regard to the relevant provisions in the Constitution.

- The Committee shall call a General Meeting within seven days if they receive a request to do so, where the request specifies a valid motion and is signed by at least 20 Ordinary Members.

- If the motion specified under the previous clause is a motion of no confidence in a Committee Officer or Non-Committee Officer, the General Meeting so called shall be an Emergency General Meeting.

- Any General Meeting called by the Committee at the request of the Members shall be held no later than 7 days from the date on which it is called.

- If the Committee fail to comply with this obligation to call a General Meeting at the request of its members, then the members who requested the meeting may themselves give notice of a General Meeting.

- A General Meeting called in this way shall be held not more than 14 days after the date when the members first requested the meeting.

## Notice of Ordinary and Annual General Meetings

- The Committee, or, as the case may be, the relevant Members, shall give at least 7 days notice of any Ordinary or Annual General Meeting to all of the members.

- The notice of any Ordinary or Annual General Meeting shall:

	- state the time and date of the meeting;

	- give the location at which the meeting is to take place;

	- invite Ordinary Members to submit motions for discussion at the meeting.

- At least 3 days before an Ordinary or Annual General Meetings, notice shall be given of:

	- the full agenda of the meeting, including the text of all proposed motions.

	- if a motion to alter the constitution of the JCR is to be considered at the meeting, the text of the proposed alteration;

	- with the notice for the Annual General Meeting, the annual statement of accounts and Committee Officers’ annual reports, or details of where the information may be found on the JCR’s website.

	- with the notice for the final Ordinary General Meeting of an Academic Term, a request for nominations for charities which shall receive donations.

- The proceedings of any General Meeting shall not be invalidated because a member who was entitled to receive notice of the meeting did not receive it because of accidental omission by the JCR.

- Motions submitted less than 3 days before an Ordinary or Annual General Meeting may be accepted at the discretion of the Chair.

- At least 5 days before an Ordinary or Annual General Meeting, notice shall be given of any motions involving public statements by the JCR

- Motions involving public statements by the JCR submitted less than 5 days before an Ordinary or Annual General Meeting may be accepted at the discretion of the Chair.

## Notice of Emergency General Meetings

- The Committee shall give 24 hours notice of any Emergency General Meeting.

- Emergency General Meetings shall only discuss urgent time-sensitive motions.

- The notice shall:

	- include the full text of the motion to be discussed;

	- if a motion to alter the constitution of the JCR is to be considered at the meeting, the text of the proposed alteration;

	- state reasoning why the motion cannot be discussed at a later date;

	- state the time and date of the meeting;

	- give the location at which the meeting is to take place.

- If Ordinary Members call for an Emergency General Meeting under clause 2.2 with good reason that their business cannot be discussed at a later date, the meeting shall be called within 24 hours, and held with 24 hours of being called. In this clause ‘good reason’ shall be at the discretion of the Committee.

## Minutes

- The Secretary shall take full and accurate minutes of the proceeding of a General Meeting and the person Chairing the General Meeting shall sign those minutes if they are correct. These will then be published on the JCR website (like with all other minutes).

- Minutes signed in accordance with clause 5.1 are, unless the contrary is proved, evidence that:

	- the General Meeting to which they relate was duly convened and held; and

	- all proceedings recorded as having taken at the General Meeting did in fact take place

- As soon as reasonably practicable after a General Meeting, the Secretary shall make available to Members the minutes taken in accordance with clause 5.1.

## Chairing of General Meetings

- General Meetings shall be chaired by the President.

- In the event that the President is unable or unwilling to Chair any part of the meeting, for whatever reason, the Ordinary Members shall elect a Committee Officer to Chair who is both able and willing to do so.

- The Chair of a General Meeting shall not participate in debate, and shall vacate the Chair if they wish to speak on a matter.

- The Chair shall ensure that:

	- the meeting is in order at all times

	- remarks made are relevant to the debate taking place

	- no defamatory remarks are made during debate

	- no new material is introduced into a summing-up speech.

	- 5 In the event that the meeting becomes disorderly, the Chair may warn the assembled Members that he intends to vacate the Chair. Should order fail to be restored within a reasonable period, they may do so, and the meeting may be closed unless another Committee Officer is prepared to accept the Chair and is elected.

## Quorum at General Meetings

- The quorum for a General Meeting is 35 Ordinary Members (excluding the Chair).

- The General Meeting may not be begun until it is quorate.

- If the meeting has been called by or at the request of the members, and a quorum is not present within 15 minutes of the starting time specified in the notice of the meeting, the meeting is closed.

- If the meeting has been called in any other way, and a quorum is not present within 15 minutes of the starting time specified in the notice of the meeting, the Chair shall adjourn the meeting. The date, time and place at which the meeting will resume shall be notified to the JCR’s members at least seven clear days before the date on which it will resume.

- If at any time during the meeting a quorum ceases to be present, the meeting may discuss issues and make recommendations to the Committee but may not make any decisions.

- Should a General Meeting be found inquorate on three separate occasions, the meeting shall be closed.

## Adjournment of meetings

- The Chair may, with the consent of a meeting at which a quorum is present, (and shall do at the
request of the meeting) adjourn the meeting to another time and/or place. No business may be
transacted at an adjourned meeting except business which could properly have been transacted
at the original meeting.

## Order of Business

- The order for business of an Ordinary or Annual General Meeting shall be as follows:

	- Minutes of the last General Meeting

	- Officers’ Reports

	- Written and Oral Questions to Officers

	- Discussion of Motions

- During the “Officers’ Reports” section of a General Meeting, any Committee Offices or Non- Committee Offices that wish to present a report on their activities may do so.

- During the “Written and Oral Questions to Officers” section of a General Meeting, any Ordinary Member may ask questions of a Committee Office or NonCommittee Office.

	- 4 After questions have addressed to Officers, but prior to discussion of motions, the Chair shall read through the list of proposed motions, and ask for each if there is any opposition. If there is no opposition expressed, either orally or by show of hands, then the motion is passed.

- All motions to which there is opposition, or to which there have been amendments proposed, shall be discussed in the following order:

	- Amendments to the Constitution or Standing Orders

	- Charities Levy (Annual General Meeting only)

	- Motions altering Compulsory levies

	- Motions altering Optional levies

	- Motions relating to financial matters in the College or JCR

	- Motions relating to College matters

	- Motions relating to matters in Oxford

	- Motions relating to the British Isles and International Community

	- All other Motions

	- Items of Debate

- Motions arising from matters outside College, but which directly affect the students of the College shall be considered in the category relating to College matters.

- Motions that require action to be taken in addition to the alteration of an optional levy shall be considered in the category relating to all other motions.

- Emergency General Meetings may only discuss a single motion, the full text of which shall be included in the notice for the meeting. No other business may be conducted.

## Proposing Motions and Amendments

- Motions relating to the procedure of meetings may be moved without notice, but shall be binding only within the meeting in which they are passed and shall not become JCR policy.

- A motion may only properly be proposed if it is lawful and is not defamatory or vexatious.

- Withdrawal of Motions

	- After a motion has been tabled, the proposer may not withdraw it or accept any amendment to it without the consent of the meeting with a simple majority.

	- In the event that the meeting refuses the proposer the right to withdraw, another proposer may be selected from the meeting by the Chair.

	- If more than one person is willing to become the proposer and one of these is the proposer of the last successful amendment, then they shall be chosen.

	- If the original proposer of a motion feels that an amendment has altered the original motion to the point where they no longer wish to propose it, the proposer of the last successful amendment shall become the proposer for the motion as a whole.

	- If a motion is submitted and no Member wishes to propose the motion, it shall be withdrawn.

- Amendment of Motions

	- Amendments may be proposed to motions, but if not submitted in writing 24 hours in advance of the start of the meeting they are accepted only at the Chair's discretion.

	- If the proposer of the motion feels that an amendment does not significantly alter the purpose of the original amendment, he may accept it as a friendly amendment.

	- If there is no opposition to the amendment being taken as friendly, it passes. If there is opposition, it is discussed and voted on in the same manner as a normal amendment.

## Procedure of Debate

- Format for Debate

	- Proposition: A speech in proposition of the motion will be sought from the proposers of the motion.

	- Short Factual Questions: Short factual questions may be put to the proposer or to Committee Officers by Ordinary Members. The Chair should allow the recipient of the question to respond immediately.

	- Debate: Ordinary Members give speeches in proposition or opposition. The Chair decides the order in which people shall speak.

	- Summary: Summary speeches containing no new information (one in proposition and one in opposition) are given by Ordinary Members chosen by the Chair.

	- Voting: Voting on the motion takes place.

- Points of Information and Order

	- Points of information, in the form of short factual questions or points, may be offered in any speech except a summing up speech, but a speaker has the right to refuse to accept any offer of a point of information.

	- Points of order shall take precedence over all other business, except that they may not be raised during a speech or a vote unless they relate to the procedure of that vote. A point of order shall be phrased as a question to the Chair, shall relate to the conduct of the debate at that time, shall not refer to the subject matter under debate and shall contain no argument.

- Order

	- Every speaker shall address themselves solely to the Chair. No speech shall be permitted which is not strictly to a distinct motion or amendment or to a point of order or information.

	- Whenever the Chair speaks, they shall be heard in silence and any other member shall cease speaking.

	- The Chair may enforce the following time limits at their discretion:

		- Proposing speeches: 5 minutes.

		- Debate Speeches: 3 minutes.

		- Summing-up Speeches: 2 minutes.

## Voting at General Meetings

- Every member shall have one vote, though the Chair may not vote save under clause 12.5.

- Unless specified otherwise in the Constitution or Standing Orders, motions and amendments both require a simple majority to pass.

- Any motion which involves a withdrawal of money from JCR reserves will require a strict two- thirds majority to pass.

- A motion put to a vote shall be decided through a show of hands, unless an alternative method is requested by means of a procedural motion.

- No motion or amendment may be voted on unless there is at least one Ordinary Member to propose it.

- In the event of an equality of votes, the Chair shall have the casting vote.

- Any objection to the qualification of any voter shall be raised at the meeting at which the vote is cast, and the decision of the Chair of the meeting shall be final.

- At the request of ten Ordinary Members, a vote may be retaken or recounted.

## Procedural Motions

- Procedural motions may be raised as a point of order under clause 11.2.b).

- Only procedural motions defined in this section may be raised.

- The time available for debate on procedural motions is at the discretion of the Chair, but shall involve the opportunity for at least one speech in favour and at least one against.

- The following procedural motions pass automatically, and are not subject to debate or vote:

	- That Quorum be counted.

- The following procedural motions pass if a two-thirds majority is in favour, and have precedence over other procedural motions:

	- That the meeting has no confidence in the Chair. The Chair shall vacate his position for the duration of the motion, and if the motion passes may not resume the Chair for the duration of the meeting.

	- That the Chair's ruling be overturned. The Chair shall vacate his position for the duration of the motion.

	- That the meeting be adjourned or closed.

	- That all or part of Standing Orders be suspended for a specified duration or until the end of the General Meeting, whichever is sooner.

- The following procedural motions pass if a two-thirds majority is in favour:

	- That the meeting move to a vote on the question. In the event that the motion is passed, the meeting shall move to summing-up speeches. This motion may only be put if there has been at least one speech for and one speech against the motion, unless there is no voting opposition. The Chair may refuse to accept a move to a vote if there has been a previous move to a vote within the last 5 minutes.

	- That the question be not put. The proposer of the item in question shall have the opportunity to speak in opposition.

	- That the meeting be held in camera for a specified period.

	- That a non-member of the JCR be invited to attend the meeting or to speak at the meeting.

	- That the question be taken in parts. If passed, each part shall be debated separately, requiring a simple majority to be passed. A debate shall then take place on the substantive motion, again requiring a simple majority to be passed.

	- That the matter be referred to the Committee or to a sub-committee.

- The following procedural motions pass if one quarter of those present and voting are in favour:

	- That the voting be accomplished by means of a secret ballot. The Secretary is responsible for the preparation of the ballot papers, and the Chair is responsible for the counting of ballots. Ordinary Members may be selected by the Chair to assist in the counting, subject to the approval of the General Meeting.

	- That the vote of the members of the Committee be recorded.

	- That a motion be moved up or down the agenda.

## Spending Motions

- All GM motions proposing funds to be withdrawn from the JCR General Fund to reimburse spending fall subject to the following rules:

	- requests for amounts from £0.01 to £200 require a simple majority;

	- requests for amounts from £200.01 to £500 require a 60% majority to pass;

	- requests for amounts from £500.01 to £10,000 require a 75% majority to pass;

	- requests of £10,000.01 and more require a 90% majority to pass; and

- All GM motions proposing funds to be withdrawn from the JCR General Fund to reimburse money spent within the current term that backdates more than two weeks fall subject to the following rules:

	- requests for amounts from £0.01 to £500 require a 60% majority to pass;

	- requests for amounts from £500.01 to £10,000 require a 75% majority to pass;

	- requests of £10,000.01 and more require a 90% majority to pass; and

- Any GM motion proposing funds to be withdrawn from the JCR General Fund to reimburse money spent before the current term fall subject to the following rules:

	- requests for amounts from £0.01 to £10,000 require a 75% majority to pass;

	- requests of £10,000.01 and more require a 90% majority to pass; and

- GM spending motions should contain details of the benefits the spending will bring to the JCR.

- The Treasurer has no power to prevent a Member proposing a spending motion. Members who propose spending motions should have a discussion with the Treasurer prior to proposal of the motion if:

	- the motion requests more than £200; or

	- the motion requests reimbursement of any spending that backdates more than two weeks.

	- If deemed necessary, without prior discussion with the Treasurer, the Treasurer can postpone the motion to the following GM.

# PROCEDURE FOR ELECTIONS

## Returning Officer

- As soon as notice of an election is given, the President shall appoint a Returning Officer, who shall be a Fellow of the College.

- In the case of elections to the post of President or Treasurer, the Returning Officer shall be the Master or the Master’s designee; the said Fellow shall be Deputy Returning Officer and has the power to act on the Returning Officer’s behalf.

- The Returning Officer is responsible for ensuring the free and fair conduct of elections, and shall have powers to:

	- alter the timetable of the election in any respect;

	- disqualify a candidate either before the start of polling or after the end of polling;

	- cancel or annul the election for any office or offices and order a new election or elections.

- The Returning Officer shall supervise in person the writing of the questions, the dissemination of voter codes (if used), the opening of the ballot boxes (if used) and counting of the votes.

- Appeals may be made against the Returning Officer’s decisions to the Master or the Master’s designee. Appeals following disqualifications may only be made with written permission from the disqualified candidate. In the case of appeal against disqualification before the poll, the Returning Officer may reinstate the candidate if they deem that insufficient time remains for full investigation before polling starts. 1.6 After the close of nominations, the President and Treasurer shall serve as Assistant Returning Officers. If either candidate is in a contested election, the Committee shall appoint in their place another Committee Officer if possible, or an Ordinary Member if not.

## Timing of Elections

- Notice of elections shall be given at least two weeks in advance of the day of the election to all Ordinary Members, including those on their year abroad.

- If it becomes necessary or desirable to change the date on which an election is held, the Returning Officer shall decide the day to which the election shall be moved, giving at least 96 hours’ notice shall be given.

- Unless specified otherwise in this Constitution or Standing Orders, all elections shall take place on the 6th Saturday of Michaelmas Full Term.

- Nominations shall be presented in person or in writing to the President, Secretary or Treasurer no later than 6pm on the 7th day before the election. The President, Secretary or Treasurer shall remain in the JCR office between 5pm and 6pm on the said day for the purpose of receiving nominations.

- If, at the close of nominations, there are not more nominations than vacancies for an office, nominations for that office shall remain open for a further 24 hours and the JCR shall be given notice of this fact.

## Election Materials and Campaigning

- No election material whatsoever may be produced before the close of nominations.

- All candidates are entitled to produce a manifesto, limited to one A4 page in length.

- A copy of each manifesto shall be displayed on the JCR noticeboard and emailed to all Ordinary Members.

- If a candidate knows beforehand that they will be unable to complete their whole term, they shall clearly state this in their manifesto.

- No other publicity material may be produced by candidates.

- Intimidation, coercion, and canvassing of voters are strictly forbidden.

- Candidates may not, in their manifesto or otherwise, express support for another candidate.

- The Secretary shall be responsible for publicising the election and preparing voting papers, and shall carry out any other duties determined by the Returning Officer. If the Secretary is a candidate for a contested election, the Committee shall appoint in their place another Committee Officer if possible, or an Ordinary Member if not.

## Hustings

- Hustings shall occur in the Norway Room. No other election meetings may be held.

- Hustings shall be chaired by the President, or in their absence by a Committee Officer specified by the President.

- Hustings for elections in Michaelmas Term shall, so far as practicable, take place over three separate (though not necessarily consecutive) days as follows:

	- First day: Presidential husts only

	- Second day: All Non-Committee Offices, and Committee Offices not specified below

	- Third day: The Entz Officer, the Foody, the Lindsay, the Secretary, the Treasurer the Vice-President (Administrative) and the Vice-President (Organisational).

- The order of hustings on the second day shall be Committee Offices and NonCommittee Offices interspersed, the exact order being determined by the Chair of the husts.

- The order of hustings on the third day is as listed above.

- The order and distribution of husts across the second two days may be altered at the Chair’s discretion if candidates are unavailable.

- The procedure for a hust is as follows:

	- Each candidate begins with an introductory speech of no more than three minutes in length.

	- Ordinary Members are then free to ask questions. Questions are subject to the approval of the Chair, and shall be directed to all candidates rather than specific candidates.

	- Each candidate is allowed no more than two minutes to respond to a question.

	- Time allocations shall be shared between candidates running for dual nomination.

	- Time allocations may be reduced at the discretion of the Chair, provided the change is applied to all candidates equally.

	- There is no limit to the number of questions that can be asked.

	- The Chair may move to the next hust if they feel that the questions have stagnated or that no progress is being made.

	- Any member of the audience who objects to the Chair’s decision to move on may call for a show of hands, with a simple majority necessary to overrule the Chair.

	- The Chair may propose that any of a) through e) be altered, either for the length of an individual hust or for all husts that day. If there are objections, a two-thirds majority is required to pass the alteration.

## Voting

- Adequate notice of the voting period shall be given to all Ordinary Members, including those on their year abroad.

- Voting will take place from 9:30am to 9pm either:

	- online, using an appropriate online system; or

	- in the Norway Room by secret ballot, except when votes are cast under clause 5.3.

- Ordinary Members on their year abroad will cast their vote either;

	- online, in the same manner as 2.a of this subsection;

	- by means of an email to the Returning Officer in dictating their preferences. The Returning Officer shall fill out paper ballot papers accordingly.

- If used, a ballot box shall be provided in the Norway Room, where casting of votes shall be supervised by scrutineers appointed by the JCR Committee.

- No candidates may act as scrutineers, and no scrutineers may canvass at the box for any candidates.

- If voting is to be done by paper ballot, voters shall provide identification to the satisfaction of the scrutineer and shall sign the register of voters provided.

- Only Ordinary Members who self-identify partially or wholly as women may vote for the post of Women’s Officer.

- Only individuals who are disabled, international, from an ethnic minority background, or who identify as LGBTQ should vote for the positions of Student Disability Officer, International Students’ Officer, Ethnic Minorities Officer, or LGBTQ Officer, respectively.

- The names of the candidates shall appear either;

	- (if online voting is used) in a randomised order as far as the software will allow, followed by the option to vote for reopening nominations; or

	- (if paper ballot is used) in alphabetical order on the ballot paper, followed by the option to vote for reopening nominations.

- Provision shall be made on the day before the election for early voting by Ordinary Members who are unable to vote on the day of the election.

- Votes shall be counted according to Single Transferable Vote in accordance with ERS97.

- Any Ordinary Member wishing to claim an infringement of these rules in connection with the election shall lodge a complaint with the Returning Officer or the President within 24 hours.

- Only the Returning Officer and the Committee are permitted to attend the counting of the votes. They shall stay for the entire duration unless given permission to do otherwise by the Returning Officer, who is in charge of all aspects of the count and whose discretion is final.

## Co-Options

- The procedure for co-option is as follows:

	- Notice of the vacancy is advertised to Ordinary Members, and those eligible and willing to take on the duties of that position nominate themselves to the Committee.

	- The Committee may then ask for a presentation, either written or oral, from the member as to their suitability for the role.

	- The Committee shall then elect a candidate from among the nominees, either by simple majority from a show of hands or by a secret ballot using the Alternative Vote method.

	- The Ordinary Member elected shall take office immediately.

## Election of Committee Offices

- Elections for the position of President shall take place on the 4th Saturday of Michaelmas Term each academic year.

- Elections for other Committee Offices shall take place on the 6th Saturday of Michaelmas Term each academic year.

- Upon the declaration of the results of the election, those elected shall be considered the Committee-Elect.

- At the final Committee Meeting of Michaelmas Term, all Committee Officers shall resign their positions. The Committee-Elect will then take office, and assume all duties of their respective positions.

- In the case of the Access and Admissions Officers, there shall be an extended handover lasting until the end of the interview period, during which the Officers and the Officers-Elect shall work together to help run the interview process.

## Election of Non-Committee Offices

- At the beginning of Trinity term, a referendum shall be held to decide whether Members wish to hold a ball or a garden party at the end of the following academic year;

- In the event that members wish to hold a ball at the end of the following academic year, the following Non-Committee Offices are to be elected in Trinity term of the academic year:

	- Ball President

	- Ball Treasurer

	- Ball Secretary

	- Ball Publicity Officer

	- JCR Ball Representatives

	- MCR Ball Representatives These elections are to be held after the Ball in all cases except that of the Ball President election, which shall be held in second week of trinity term.

- These offices shall take over responsibility from their predecessors on Saturday of 8th Week of the Trinity term in which they are elected.

- In the event that members wish to hold a garden party at the end of the following academic year, the following Non-Committee Offices are to be elected in Trinity term of the academic year:

	- Garden Party President

	- Garden Party Treasurer

	- Garden Party Secretary

	- Garden Party Publicity Officer

	- JCR Garden Party Representatives

	- MCR Garden Party Representatives These elections are to be held after the Garden Party in all cases except that of the Garden Party President election, which shall be held in second week of trinity term.

- These offices shall take over responsibility from their predecessors on Saturday of 8th Week of the Trinity term in which they are elected.’

- The elections detailed in 8.2-8.4 will not take place if the JCR has voted not to hold a ball or garden party in the year in consideration, as in 8.1.
