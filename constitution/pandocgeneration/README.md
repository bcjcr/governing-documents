# Guide to generating

pandoc constitution.md --template=custom_template.tex -o constitution.pdf

useful commands to change numbering to bullets change as appropriate used in vim

:%s/^\( \{4\}\)\d\+\.\s*/\t-/

:%s/^\s\{0,4\}\(\d\+\.\)\{2\}\s*/- /

second one does 2.2. etc... changes to just -(whitespace)

first one does 4 whitespace and then replaces with tab-(whitespace)

all the part headings need to be level 1 in md so #

and then subheadings need to be level 2 in md so ##

table of contents in latex needs to be set level 1 to print only parts.

some work needed to ensure numbering looks nice...

and that hyperlinks are generated for the subparts...


