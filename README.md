# Governing Documents

Contains the Governing Documents of the JCR.

## Modifying the Documents

Edit the markdown files inside the `pandocgeneration/` directory of each document.

From `cspandocgeneration`, run `pandoc IN_NAME.md --template=custom_template.tex -o OUT_NAME.pdf` for the relevant `NAME`s to generate a properly numbered and formatted PDF.

The static links from the website to these documents (e.g. [this one](https://gitlab.com/bcjcr/governing-documents/-/raw/main/constitution/pandocgeneration/constitution.pdf)) should not need to be changed, but some may reference `Balliol-JCR-Constitution.pdf` (deprecated) so perhaps just copy the pandoc output here too.
