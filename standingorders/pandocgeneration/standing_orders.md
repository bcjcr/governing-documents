# PRELIMINARY

## Definitions

- The Constitution defines the terms and phrases used herein, and should be used to interpret the Standing Orders correctly.

## Interpretation

- The interpretation of these Standing Orders shall be governed by Part I section 2 of the Constitution, mutatis mutandis.
## Notice

- Where a provision of these Standing Orders requires that notice of any matter be given to Members, the giving of notice shall be governed by Part I section 3 of the Constitution, mutatis mutandis.

# THE JCR

## Societies

- For the purposes of this section, Societies are unofficial bodies dedicated to a particular interest or activity that are comprised primarily of Members.

- Societies refused permission to battel Members by the College may apply to the Committee for permission to do so, which may be granted or withheld at the Committee’s discretion.

- All Societies are legally liable for damage caused by their members to College or JCR property, provided that the damage is deemed to have occurred in the pursuit of the Society’s interests.

## Standing Policy

- Standing Policy reflects the long-term beliefs and aims of the JCR.

- An item of Standing Policy may be added at any time by means of a motion passed at a General Meeting, subject to clause 2.3.

- An item of Standing Policy may only be added if it is lawful and not defamatory, frivolous, vexatious or in any way conflicts with a suﬀiciently strong commitment to Net Zero by 2050. An item shall be:

	- by default deemed not in conflict;

	- deemed conflicting if a Committee vote following an assessment of the evidence for and against decides it to be so;

	- deemed conflicting if a GM motion is passed overruling a decision by the Committee that it is not in conflict; and

	- deemed not in conflict if a GM motion is passed overruling a decision by the Committee that it does conflict.

- An item of Standing Policy may be stricken or amended by means of a motion passed by a two-thirds majority at a General Meeting.

- A record of Standing Policy shall be maintained by the JCR Secretary.

## JCR Annual Report

- During fifth week of Michaelmas Term the President shall, with respect to the previous Financial Year, prepare, in draft form, an annual report on the JCR’s activities that shall:

	- set out the main activities undertaken by the JCR to further the Objects and the main achievements of the JCR

- During sixth week of Michaelmas Term:

	- the President shall submit the draft annual report prepared in accordance with clause 3.1 to a Committee Meeting; and

	- at a Committee Meeting, the Committee shall, by resolution:

		- approve the draft report, with any alterations that the Committee may, by resolution, make; and

		- authorise the President to sign and date the finalised report.

- The annual report approved and signed in accordance with clause 3.1 is the JCR Annual Report for the JCR Financial Year to which it relates.

- The President shall:

	- submit the JCR Annual Report to:

		- the Annual General Meeting; and

		- the executive committee of the College.

	- make the JCR Annual Report available to:

		- any member who requests to inspect it; and

		- any member of the public who requests, in writing, a copy of it and who pays any reasonable fee to cover the cost of complying with that request.

	- ensure that the JCR Annual Reports are preserved for at least six years from the end of the JCR Financial Year to which they relate.

## JCR Annual Statement of Accounts and Independent Examination

- As soon as practicable after the end of the JCR Financial Year, the Treasurer shall, in respect of that JCR Financial Year, prepare, in draft form, an annual statement of accounts that shallinclude:

	- a statement of financial activities showing the total incoming resources and application of the resources, together with any other movements in the total resources, of the JCR; and

	- a balance sheet showing the state of affairs of the JCR as at the end of the JCR Financial Year; and

	- a list of the external organisations to which the JCR has made donations, together with details of those donations.

- During fifth week of Michaelmas Term:

	- the Treasurer shall submit the draft annual statement of accounts prepared in accordance with clause 4.1 to a meeting of the Finance Subcommittee; and

	- at a meeting of the Finance Subcommittee, the Finance Subcommittee shall, by resolution:

		- approve the draft report, with any alterations that the Finance Subcommittee may, by resolution, make; and

		- authorise the Finance Subcommittee Members present to sign and date the finalised annual statement of accounts; and

		- appoint, on the advice of the JCR Office Manager, an independent examiner who has the requisite ability, practical experience and professional standing to carry out a competent examination of the JCR Annual Statement of Accounts.

- The Treasurer shall:

	- submit the JCR Annual Statement of Accounts to:

		- the Annual General Meeting; and

		- the executive committee of the College.

	- make the JCR Annual Statement of Accounts available to:

		- any student who requests to inspect it; and

		- any member of the public who requests, in writing, a copy of it and who pays any reasonable fee that the Committee may resolve to charge to cover the cost of complying with that request; and

	- submit the JCR Annual Statement of Accounts to be examined and reported on by the independent examiner appointed under subclause 4.1 in accordance with applicable legislation; and

	- ensure that JCR Annual Statements of Accounts and the associated reports of the independent examiner are preserved for at least six years from the end of the JCR Financial Year to which they relate.

## Discipline

- Members are subject to disciplinary actions issued by the Master or the Master’s representative.

- In the event that an affected Member believes a disciplinary action to be unfair, excessively harsh or unmerited, they may appeal to the Committee.

- The Committee shall judge whether the complaint is valid, and, if necessary, identify and provide an effective remedy.

# COMMITTEE OFFICES

## Duties of Committee Offices

- The following sections detail additional duties imposed upon each Committee Office. 

- These duties are binding upon each Committee Office and the Committee Officer that holds them.

## Duties of the President

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the President is ultimately responsible for the management of the day-to-day affairs of the JCR.

- Without limiting the generality of the previous clause, the President shall:

	- represent the JCR, in person or by nominee, to the College, or other outside bodies as the Committee shall decide;

	- ensure that the management of the JCR is at all times conducted in an open and transparent manner, and to this end ensure that there is regular and comprehensive communication between the Committee and the Members;

	- ensure that the JCR complies with these Standing Orders at all times; and

	- Chair the General Meetings of the JCR;

	- prepare a report to be presented to the JCR prior to each General Meeting;

	- co-ordinate and supervise the duties of Committee Offices in accordance with these Standing Orders;

	- co-ordinate, and be ultimately responsible for, the writing of the JCR Annual Report.

## Duties of the Vice President (Administrative)

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Vice President (Administrative) shall cooperate with the President in the management of the day-to- day affairs of the JCR.

- Without limiting the generality of the previous clause, the Vice President (Administrative) shall:

	- represent the views and concerns of Members to all appropriate parties, including, but not limited to, the Committee, the President and College;

	- ensure that the management of the JCR is at all times conducted in an open and transparent manner, and to this end ensure there is regular and comprehensive communication between the Committee and the Members;

	- chair all meetings of Finance Subcommittee;

	- chair all meetings of Executive Subcommittee

	- deputise for the President in all their duties, at the President’s request or in their absence.

	- Duties of the Vice-President (Organisational)

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Vice President (Organisational) shall be responsible for the efficient organisation of Freshers’ Week and other miscellaneous events within the college and the JCR.

- Without limiting the generality of the previous clause, the Vice President (Organisational) shall:

	- chair the Freshers’ Week Subcommittee

	- represent the JCR to College on all matters relating to Freshers’ Week

	- be responsible for the oversight of bops and the Entz officers’ activities

	- organise and manage the ‘June Jamboree’, the JCR’s garden party to be held annually in Trinity term

## Duties of the Secretary

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Secretary is ultimately responsible for recording of JCR business and the preservation of these records.

- Without limiting the generality of the previous clause, the Secretary shall:

	- maintain a current copy of the Constitution and Standing Orders and ensure it is available to all members of the JCR, whether in paper or electronic form;

	- maintain a register of all Ordinary and Associate Members of the JCR;

	- maintain a list of JCR Heroes and Villains, to which items may be added by means of a motion passed by a simple majority at a General Meeting;

	- send an email prior to the charities GM containing the nominated charities and web-links, and offer to amend descriptions prior to the GM, and to put these descriptions up in the JCR;

	- ensure each Committee Office produces both termly and annual reports, and that these reports are:

		- presented to the General Meeting at the end of each term and the Annual General Meeting, respectively; and

		- preserved for at least six years after their creation

	- minute all meetings of Committee Members including, but not limited to:

		- all General Meetings;

		- Committee Meetings;

		- Finance Subcommittee meetings;

		- meetings of the Freshers’ Week Subcommittee;

	- make these minutes freely available to members of the JCR in a timely fashion;

	- publicise the meetings and activities of the JCR;

	- provide services and entertainment at General Meetings, for which each General Meeting has a budget of £25;

	- organise, in conjunction with the Amalgamated Sports Rep the annual Committee and sports team photos;

	- organise the annual Teachers Excellence Awards together with the Awards Subcommittee;

	- manage the up keep of the JCR office.

## Duties of the Treasurer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Treasurer is responsible for the management of the JCR finances in general and the welfare of all employees of the JCR, excluding those of the Lindsay Bar.

- Without limiting the generality of the previous clause, the Treasurer shall:

	- submit a report of the financial position of the JCR to the College once every Full Term;

	- liaise with responsible Committee Offices and Offices concerning financial aspects of the JCR’s activities;

	- represent the JCR on financial matters to the College, or other outside bodies as the Committee shall decide;

	- supervise and assist the JCR Office Manager in:

		- preparation of the JCR’s accounts;

		- handling of petty cash;

		- payment of fees and licences owed by the JCR;

		- reimbursement of authorised expenses incurred by Committee Offices and Offices on behalf of the JCR.

	- project the bar’s profits for the calendar year at the end of both Hilary and Trinity using a reasonable and transparent system;

	- oversee the Foody’s projection of yearly loss in their termly report.

- The Treasurer shall prepare and publish on the JCR website a list of what constitutes ordinary expenditure, with examples of items purchased under each category which can be added to or be edited by the GM and the Treasurer is allowed to maintain executive power over whatconstitutes ordinary expenditure only in instances where the expenditure is time sensitive, and on the condition that they bring a report from officers to the next GM following said emergency expenditure explaining the reasons for bypass of the GM.

## Duties of the Lindsay

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Lindsay shall:

	- manage the finances and utilisation of the Lindsay Bar, for which they are solely responsible;

	- ensure the efficient day-to-day running of the Lindsay Bar and its services such as bops and other events in accordance with the Club Rules;

	- be responsible for the welfare and provision of the bar staff;

	- represent the JCR to the College on matters relating to alcohol and the use of the Lindsay Bar;

	- give the Ball Subcommittee any assistance required in alcohol sales at the Ball.

- The Lindsay must make a profit of £1000 in their (calendar) year.

	- The Lindsay must implement price rises as agreed in GM;

		- if the bar is projected at the end of Hilary to make a profit of less than £0 over the calendar year; or

		- if the bar is projected at the end of Trinity to make a profit of less than £1,000 over the calendar year.

	- Price rises must be checked and approved by a committee of the Lindsay, Duckworths, Treasurer, and President; and

	- the Lindsay must bring to a GM and get approval to buy any capital expenditure over £50 such that: (bar savings account + current account – proposed expenditure)<£20,000.

- The Lindsay must ask the JCR for loans through a GM spending motion whenever it wants to engage in spending that it cannot afford on its own.

	- Spending of:

		- £0.01 to £200 requires a simple majority at one GM to pass;

		- £200.01 to £500 requires a 60% majority at one GM to pass;

		- £500.01 and above requires 75% majority at one GM to pass.

	- Proposals that commit to yearly spending must pass the majority rules in subsection a) at two GMs rather than one; and

	- GM spending proposals should contain the following:

		- the benefits the proposal will bring to the JCR; and

		- a spending plan which includes how the money will be spent and projections of profit, if relevant.

## Duties of the Foody

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Foody shall:

	- Chair the Standing Subcommittee;

	- manage the finances and utilisation of Pantry, for which they are solely responsible;

	- ensure the efficient day-to-day running of Pantry and its services, including the provision of last minute cover for shifts;

	- be responsible for the welfare and provision of the Pantry staff;

	- represent the JCR to the College on matters relating to food and the use of Pantry;

	- produce a termly report which includes financial details like a projection of their yearly loss, using some sensible and transparent method.

	- Check on the training standards of PSC members.

- Each Foody is to be mandated to have a financial target, which is to not to lose more than £1200 per year.

	- If the foodie’s projection of their yearly loss exceeds this target two terms in a row, one of two actions must be taken, agreed at the discretion of the foodies and the treasurer, and presented by both to the JCR:

		- either, a paper must be published by the foodies detailing how profits will be increased over the next term and by how much, or

		- prices must be raised by some non-negligible amount.

	- If the Michaelmas term figures are not an improvement, or not a sufficiently large improvement to meet the target, prices must be raised.

## Duties of the Well-being and Health Officer (Dr. WHO)

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Dr. WHO shall:

	- Chair the meetings of the Welfare Subcommittee;

	- co-ordinate the activities of the welfare-related Committee Offices;

	- manage the general logistics and distribution of welfare provisions, assisted by other welfare-related Committee Offices as necessary.

	- provide JCR members with assistance and information on matters of welfare and housing;

	- represent the JCR to the college on issues of welfare and anti-discrimination.

## Duties of the Housing Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Housing Officer shall:

	- represent the JCR to the college on issues of welfare, housing and anti-discrimination;

	- organise the housing ballot and room choosing for College Mainsite and Jowett Walk, ensuring that:

		- all rising first year students shall live in College Mainsite; and

		- all rising third and fourth years shall live in College Mainsite or Jowett Walk, unless they should choose otherwise; and

		- all undergraduates shall be able to live in College-owned accommodation for two years of their degree; and

		- undergraduates shall be prioritised according to the number of times they have lived in College Mainsite, with a lower number granting higher priority; and

		- undergraduates who ballot for Jowett Walk shall be in groups no larger than 8, and the ballot position of each group shall be the mean of the group’s members’ ballot positions.

	- maintain a current list of college rooms, and their prices;

	- Update and maintain a housing database;

	- Organise a housing meeting for first years in Michaelmas;

	- Liaise with OUSU to support students in housing related issues;

	- Liaise with College on housing policy;

	- Liaise with the Welfare Officer and Academic Affairs Officer on relevant issues;

	- Organise a termly housing working group meeting open to all member of the JCR.

- Housing Officer shall use a system (possibly DPRS) to keep track of room popularity and shall assess the pricing of rooms before each year’s rent negotiations and to propose new pricing when appropriate.

## Duties of the Academic Affairs and Careers Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Academic Affairs and Careers Officer shall:

	- represent the JCR on the the Oxford University Student Union Academic Affairs Committee;

	- provide all members of the JCR with assistance and information on Academic Affairs and Careers in general; and

	- lobby for JCR representation on all relevant college academic committees.

- The Academic Affairs and Careers Officer has a termly budget of £50, which shall be used in accordance with part IV, section 5 of the Constitution.

## Duties of the Access and Admissions Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Access and Admissions Officer shall:

	- liaise with the the Oxford University Student Union’s VP Access and Academic affairs and College Admissions staff regarding access;

	- promote both college-based and University-wide access schemes and events and co- ordinate access initiatives run by Balliol students;

	- liaise with the College Admissions staff to co-ordinate student involvement in College tours, Open Days and Interviews;

	- rewrite the Alternative Prospectus biannually;

	- organise entertainment for candidates during interviews and for those staying overnight during Open Days; and

	- represent the JCR’s opinions on matters pertaining to Admissions to the appropriate persons.

## Duties of the Affiliations Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Affiliations Officer shall:

	- represent the JCR to the Oxford University Student Union and the MCR;

	- inform JCR members of the agenda for the Oxford University Student Union Council;

	- inform JCR members of the results of the Oxford University Student Union Council votes;

	- if they feel unable to adequately represent the JCR’s opinion to a matter on the Oxford University Student Union Council, bring to the next General Meeting a motion to establish the opinion of the JCR using the following method:

		- Calculate the net number of votes for (or against, mutatis mutandis) the motion as a percentage of the total number of votes

		- Vote in the Oxford University Student Union Council on this matter as follows:

        	- If the percentage is higher than 70%, a net vote of three for the motion shall be cast, otherwise;

        	- if the percentage is higher than 40%, a net vote of two for the motion shall be cast, otherwise;

        	- if the percentage is higher than 10%, a net vote of one for the motion shall be cast, otherwise;

        	- if the percentage is below 10%, a net vote of zero for the motion shall be cast

		- The exact combination of votes used to achieve these net values is left to the discretion of the Affiliations Officers.

	- inform JCR members of dual-common room events being put on by the MCR; and

	- organise joint JCR-MCR events.

## Duties of the Charities and RAG Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Charities and RAG Officer shall:

	- manage the JCR charities account, ensuring that at least one motion is brought to a General Meeting each term giving money from that account to charity;

	- at least seven days prior to the online ballot, invite Ordinary Members to nominate charities to receive donations. Nominated charities must be registered charities in England and Scotland;

	- organise the online ballot for the nominated charities, allowing members to select up to three (3) preferences, with first references scoring 3 ‘points’, second 2, and third 1, and divide the money not covered by clause d) of this subsection proportionally to the points scored by each charity at their discretion;

	- ensure that a reserve fund of £500 is maintained in the Charities fund at all times, to be used for emergency charitable motions brought under part VII, clause 1.4 b) of the Constitution;

	- liaise with Oxford RAG in order to co-ordinate and promote events both within Balliol and across the university;

	- liaise with the university RAG representatives to expand and coordinate Balliol JCR's involvement in RAG week;

	- bring a motion to the Annual General Meeting to determine which charities shall be funded by means of an optional levy for the next academic year, together with a list of the charities currently included on the levy, why they are there, and for how many terms they have been included.

## Duties of the Design and Maintenance Officer (D&M)

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Design and Maintenance Officer shall maintain all JCR-administered fittings and furniture.

## Duties of the Student Disabilities Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Student Disabilities Officer shall:

	- provide disabled members of the JCR with specific assistance, information and support beyond the responsibilities of the Dr WHO;

	- represent the interests of all disabled members of the JCR; and

	- raise awareness of mental health issues and work to encourage disabled students to apply to Balliol.

- The Disabled Students Officer has a termly budget of £50, which shall be used in accordance with part IV, section 5 of the Constitution.

## Duties of the Entz Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Entz Officer shall:

	- plan, organise and promote the social and entertainment activities of the JCR;

	- at their discretion convene and Chair meetings of the Entz Subcommittee;

	- maintain and manage the Entz cupboard;

	- organise regular quizzes in the Lindsay Bar;

	- organise regular bops in the Norway Room with a termly budget of £300 multiplied by the number of bops in that term, treating repairs and maintenance of already owned Entz equipment as ordinary expenditure but requiring the express permission of the Treasurer; and

	- represent the JCR to the College on matters of entertainment.

- The Entz Officer must liaise with the Treasurer before purchasing any club night tickets for Freshers Week.

- The Entz Officer must treat other prospective expenditure, such as purchase of new equipment, as non-ordinary, meaning it must be passed through either a GM or Committee Lunch before it can be purchased, except where this expenditure is of a time-sensitive nature.

## Duties of the Environment and Ethical Investment Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Environment and Ethical Investment Officer shall:

	- engage with catering staff to improve the environmental and ethical standards of food served within College;

	- engage with college gardeners to enhance biodiversity within Balliol;

	- monitor the investment of the college’s endowment, and lobby college to ensure that investments are in the best interests of students and wider society;

	- monitor and seek to improve recycling facilities within college;

	- co-operate with other influential bodies (such as Balliol MCR) when E&E campaigns are likely to be more successful by doing so.

- The Environment and Ethics Officer has a termly budget of £50, which shall be used in accordance with part IV, section 5 of the Constitution.

- The Environment and Ethics Officer/s shall be a key party responsible for organising and controlling the JCR Green Fund. The fund exists to provide monetary assistance to any event, project or activity that seeks to have a direct positive environmental impact in Balliol and operates under the following rules:

	- the JCR allocates £10 at the start of every term to the green fund;

	- any money the JCR sets aside to be used for environmentally positive purposes shall be automatically deemed a part of the fund;

	- applications made for assistance from the fund shall be decided on a case-by-case basis;

	- any money remaining in the fund at the end of week 9 of every term shall be spent on high-quality carbon credits; and

	- the Treasurer can prevent an award from the fund only if it will cause the fund to enter a deficit.

## Duties of the Ethnic Minorities Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Ethnic Minorities Officer shall:

	- represent the interests of Balliol’s ethnic minorities students;

	- work with the Access and Admissions Officer and the Oxford Access Scheme in order to raise the profile of Balliol among ethnic minority applicants.

- The Ethnic Minorities Officer has a termly budget of £50, which shall be used in accordance with part IV, section 5 of the Constitution.

## Duties of the International Students Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the International Students Officer shall:

	- provide international members of the JCR with specific assistance, information and support beyond the responsibilities of the Dr WHO; and

	- represent the interests of Balliol's international students; and

	- manage and maintain the international storage room.

- The International Students Officer has a termly budget of £50, which shall be used in accordance with part IV, section 5 of the Constitution.

## Duties of the Johns de Balliol

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Johns de Balliol shall:

	- produce and distribute ‘John de Balliol’, which shall be the JCR news and entertainment publication; and

	- write and direct the JCR Christmas pantomime.

- The John de Balliol shall take part in the vetting process agreed between the President, Vice- Presidents, Welfare Subcommittee and John de Balliol as detailed under the duties of welfare subcommittee with the following differences:

	- The Officers will send out an email 1 week before each publication explaining:
		- that people will be included;
		- that they can ask for more information, say no, or give a blanket consent;
		- that welfare can be contacted if they do not feel comfortable with J de B’s;
		- that nothing will go in without their express permission;
		- that submissions are required;
		- that J de B prepare a publication including only stories that have received consent; and
		- that vetting will occur as per usual.

	- Vetting will take place in the Norway Room, and the Officers come to introduce and present the publication. The Officers are not to be present during the vetting process and have no opportunity to question a decision: they are simply to be told that something has been removed and that it cannot be included.

	- The publication will be circulated via email to the JCR and copies will be printed and left in the Norway Room.

## Duties of the LGBTQ+ Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the LGBTQ+ Officer shall:

	- provide LGBTQ+ members of the JCR with specific assistance, information and support beyond the responsibilities of the Dr WHO;

	- provide JCR members with information and advice on LGBTQ+ matters;

	- help LGBTQ+, questioning and straight JCR members with problems of sexuality, sexual orientation and gender identity;

	- liaise with University-wide LGBTQ+ bodies, such as the Oxford University Student Union LGBTQ+ Council;

	- access the sanitary fund and have the responsibility of reimbursing and providing sanitary products; and

	- update the definition of transphobia, as recorded in the JCR Standing Policy, as required.

- The LGBTQ+ Officer has a termly budget of £100, which shall be used in accordance with part IV, section 5 of the Constitution.

- The LGBTQ+ Officer is responsible for organising and controlling the JCR Gender Expression Fund. The fund exists to reimburse students who purchase items to make them more comfortable with their gender presentation and operates under the following rules:

	- the JCR allocates £100 a term to the gender expression fund;

	- any money in the fund at the end of a term rolls over to the following term until the fund reaches a cap of £1000;

	- up to £80 per student per term can be awarded from the fund by the LGBTQ+ Officer, on a case-by-case basis; and

	- the Treasurer can prevent an award from the fund only if it will cause the fund to enter a deficit.

## Duties of the Women’s Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Women’s Officer shall:

	- represent the interests of all members of the JCR who are women;

	- attend the the Oxford University Student Union Women's Committee and liaise with the the Oxford University Student Union’s VP Women;

	- manage the provision of emergency contraception, tampons, rape alarms and pregnancy tests; and

	- to share the responsibility of reimbursement of menstrual cups and provision of sanitary products with the LGBTQ+ Officers.

- The Women’s Officer has a termly budget of £50, which shall be used in accordance with part IV, section 5 of the Constitution.

## Duties of the Website and Computing Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Website and Computing Officer shall:

	- maintain and improve the JCR website and all other computing aspects of the JCR;

	- provide computing services for General Meetings;

	- regularly and promptly circulate a list of internal and external notices and bulletins;

	- represent the JCR on computer and internet related matters to College; and

	- maintain the JCR’s IT Policy which acts as a best practice guideline for how the JCR should act regarding IT activity.

## Duties of the Amalgamated Sports Rep

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Amalgamated Sports Rep shall:

	- deal with all sporting matters concerning the JCR;

	- maintain a list of all sports captains in College;

- Liaise with College on sporting issues, including funding; and provide the necessary assistance to the Secretary to organise the annual JCR sports teams’ photographs.

## Duties of the Class Affiliations and First Generation Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Class Affiliations and First Generation Officer shall:

	- Create an awareness, particularly during Freshers Week, of the common experience when transitioning to Oxford, i.e. through events/emails/resources;

	- provide peer support for students who encounter any negative issues as a result of their socioeconomic class;

	- provide support for students who have difficulty transitioning between home and university environments because of their socioeconomic background;

	- represent the interests of working class or first generation students or those who have lived in care or who feel that their socioeconomic background has affected their experience at Oxford University, in committee meetings and to college;

	- raise awareness of existing financial support that the college provides, and helping students to feel comfortable to ask for it if needed;

	- raise awareness of welfare issues which affect these students, providing suggestions on how those who are interested in helping are able to help;

	- reinforce Balliol JCR’s friendly environment, by encouraging existing good interclass relations within college, encouraging discussion about socioeconomic class, and appeasing tensions where there exist areas for improvement;

- Provide spaces and opportunities for these students to speak openly about their experiences.

- Act as a liaison between Balliol students and ongoing class-related OUSU campaigns.

## Duties of the Suspended Students' Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Suspended Students' Officer/s shall:

	- represent the interests of all Suspended Status members of the JCR;

	- along with the whole JCR, petition College to change their default Terms of Suspension to allow Suspended Status Students who do not pose a specific health and safety risk to other students:

		- to remain in residence until a mutually agreed appropriate date after the confirmation of suspension; and

		- to enter college premises freely without requiring specific permission.

	- support students in negotiations with College about suspension of studies, including negotiation for better Terms of Suspension where possible.

	- provide information and act as a point of contact to Suspended Status members of the JCR; and

	- learn about the lived experience of and difficulties experienced by suspended students at Balliol and compile this information in a way that can be passed down to successive Suspended Students’ Officers.

# SUPPORTING COMMITTEES

## Executive Subcommittee

- Executive Subcommittee is an advisory body, which exists to:

	- discuss any JCR matter which is inappropriate for committee lunch, finance subcommittee or welfare subcommittee;

	- discuss issues related to finance subcommittee or welfare subcommittee which are relevant to or require action from members of executive subcommittee who were not present in the first instance;

	- co-ordinate the provision of JCR services; and

	- co-ordinate negotiations and communications with college.

- The powers of Executive Subcommittee are limited; specifically it shall have no power to:

	- authorise expenditure of the JCR’s money;

	- overrule the Committee on any matter whether financial or otherwise, though it may offer advice and recommendations;

	- interfere with the day-to-day expenditure of the JCR, which shall remain at the discretion of the Treasurer;

	- make any decisions which would ordinarily require a GM.

- Executive Subcommittee comprises five Committee Offices, consisting of:

	- the President;

	- the Vice-President (Administrative);

	- the Vice-President (Organisational);

	- the Treasurer;

	- the Drs WHO;

	- the Secretary; and

	- the Lindsay

- Executive Subcommittee shall meet at least four times each term at times to be determined by the Vice-President (Administrative). 191.5. Executive Subcommittee shall be chaired by the Vice-President (Administrative), or in their absence the President. The Chair shall have the responsibility to:

	- give notice to members when and where the next meeting shall be held;

	- circulate an agenda, specifying particulars of the business to be transacted at that meeting of the Executive Subcommittee and the order in which that business is to be transacted.

- Quorum shall consist of a representative from at least four offices outlined in 1.3.

- The subcommittee shall then discuss any other relevant matters, and make recommendations to Committee Officers.

- The minutes Executive Subcommittee meetings shall be taken by the Secretary, who shall publicise them to the members of the JCR.

- Ordinary Members may attend meetings of Executive Subcommittee subject to the discretion of the Chair.

- Subject to majority approval, the Executive Subcommittee may decide to go in camera to discuss an agenda item.

	- Should the Executive Subcommittee decide to go in camera, any Ordinary Members present shall leave unless requested to stay by a majority of the Executive Subcommittee.

	- No minutes may be taken relating to discussions held in camera.

## Finance Subcommittee

- Finance Subcommittee is an advisory body, which exists to:

	- oversee the finances of the JCR;

	- co-ordinate the provision of JCR services; and

	- supervise all staffing matters in the JCR; and

	- complete all actions necessary to approve the JCR Annual Statement of Accounts, as laid out in part II, section 4 of these Standing Orders.

- The powers of Finance Subcommittee are limited; specifically it shall have no power to:

	- authorise expenditure of the JCR’s money;

	- overrule the Committee on any matter whether financial or otherwise, though it may offer advice and recommendations; and

	- interfere with the day-to-day expenditure of the JCR, which shall remain at the discretion of the Treasurer.

- Finance Subcommittee comprises five Committee Offices, consisting of:

	- the President;

	- the Vice-President (Administrative);

	- the Treasurer;

	- the Lindsay;

	- the Foody.

- Finance Subcommittee shall meet at least four times each term at times to be determined by the Vice-President (Administrative).

- Finance Subcommittee shall be chaired by the Vice-President (Administrative), or in their absence the President. The Chair shall have the responsibility to:

	- give notice to members when and where the next meeting shall be held;

	- circulate an agenda, specifying particulars of the business to be transacted at that meeting of the Finance Subcommittee and the order in which that business is to be transacted.

- Quorum shall consist of the Treasurer, the Lindsay, The Foody and one of either the President or Vice President (Administrative).

- At each meeting the Foody and Lindsay shall give a report on the financial situation of Pantry and the Lindsay Bar respectively.

- The subcommittee shall then discuss any other relevant matters, and make recommendations to Committee Officers.

- The minutes of Finance Subcommittee meetings shall be taken by the Secretary, who shall publicise them to the members of the JCR.

- Ordinary Members may attend meetings of Finance Subcommittee subject to the discretion of the Chair.

- Subject to majority approval, the Finance Subcommittee may decide to go in camera to discuss an agenda item.

	- Should the Finance Subcommittee decide to go in camera, any Ordinary Members present shall leave unless requested to stay by a majority of the Finance Subcommittee.

	- No minutes may be taken relating to discussions held in camera.


## Climate Action Subcommittee

- The Climate Action Subcommittee is a body which exists to lead the JCR’s activities related to the environment. In addition to the other duties imposed by the Constitution, Standing Orders and by the resolutions of the Committee or of General Meetings, it shall:

	- be in charge of any publications relating to the JCR’s environmental commitments;

	- act on any concerns or complaints relating to the JCR’s environmental response. This shall
include:

		- be the first party to decide on any appeals relating to decisions by the JCR in regards to its environmental response. If such an appeal is unsuccesful it can of course be made to the Committee; and

		- whether to grant any exemptions relating to rulings about the JCR’s environmental response where they are sought.

	- discuss any JCR matter which is related to its environmental commitments; and

	- assist the Environment and Ethics Officer/s with any of their duties. Specifically it shall jointly decide on matters relating to any JCR money under the control of the Officer/s.

- The powers of the Climate Action Subcommittee are limited; specifically it shall have no power to:

	- authorise expenditure of any of the JCR’s money not within its administrative purview;

	- overrule the Committee on any matters not within its remit; or

	- make any decisions which would ordinarily require a GM.

- The Climate Action Subcommittee shall comprise 6 Committee Offices, and up to another 5 JCR Ordinary Members, consisting of:

	- the Environment and Ethical Investment Officer/s;

	- the President;

	- the Vice-President (Administrative);

	- the Vice-President (Organisational);

	- the Secretary;

	- the Treasurer; and

	- up to 5 volunteers approved by the remainder of the subcommittee.

- The Climate Action Subcommittee shall meet at least twice a term at times to be determined by the Vice-President (Administrative).

- The Climate Action Subcommittee shall be chaired by one of the Environment and Ethical In- vestment Office, who must be present at all meetings. The Chair shall have the responsibility to:

	- give notice to members when and where the next meeting shall be held; and

	- circulate an agenda, specifying particulars of the business to be transacted at that meeting of the Climate Action Subcommittee and the order in which that business is to be transacted.

- There shall be no requirements for Quorum.

- The minutes at the Climate Action Subcommittee meetings shall be taken by the Secretary, who shall circulate them to all members of the JCR within one week of the meeting they describe.

- Ordinary Members may attend meetings subject to the discretion of the Chair.

- The Climate Action Subcommittee may not decide to go in camera to discuss an agenda item.

## Welfare Subcommittee

- Welfare Subcommittee is an advisory body, which exists to:

	- co-ordinate the JCR’s welfare provision;

	- deal with matters of mutual advice for those Committee Offices that have a welfare-related role; and

	- discuss issues pertaining to JCR members of a sensitive or confidential nature.

- Welfare Subcommittee shall have no power to pursue actions that:

	- may have a definite effect on the actions of the rest of the JCR Committee;

	- have a wider-than-welfare remit.

- To pursue actions which are included under clause 4.2 Welfare Subcommittee shall seek the approval of the JCR Committee.

- Welfare Subcommittee comprises ten Committee Offices, consisting of:

	- the Dr WHO;

	- the LGBTQ+ Officer;

	- the Women’s Officer;

	- the Academic Affairs and Careers Officer;

	- the Disabled Students Officer;

	- the Ethnic Minorities Officer;

	- the International Students Officer;

	- the Housing Officer;

	- the Class Affiliations and First Generation Officer;

	- the Suspended Students’ Officer.

- Welfare Subcommittee shall meet on an ad hoc basis and may meet at the request of any one of its members. It shall meet at least two times each term.

- If the Committee Office of Dr. WHO is held by two individuals, Welfare Subcommittee shall be chaired by one Dr. WHO and minuted by the other.

- If the Committee Office of Dr. WHO is held by a single individual, Welfare Subcommittee shall be chaired by Dr. WHO and minuted by a member of the subcommittee.

- Minutes of the meetings shall be made freely available to all members of the JCR.

- Ordinary Members may attend the meeting of the subcommittee subject to the discretion of the Chair.

- Subject to majority approval, the subcommittee may decide to go in camera if the welfare or behaviour of a specific JCR member is being discussed.

	- Should the subcommittee decide to go in camera, any Ordinary Members present shall leave unless requested to stay by a majority of the Welfare Subcommittee.

	- No minutes may be taken relating to discussions held in camera.

- Welfare Subcommittee has a termly budget of £250, which shall be used in accordance with part IV, section 5 of the Constitution. This budget is to be used for events exceeding the termly budgets of individual Offices on the subcommittee.

- Welfare Subcommittee shall play a part in the vetting process of John de Balliol publications in the following way:

	- The JdeB will be emailed to all members of the welfare subcommittee the day before the Committee Meeting, so that each member has an extended time to read the proposed publication to think about all possible readings of content and wider upset which could occur. Then within the Norway Room itself they can read it at their own pace, and this ensures that it is clear what has been shown to the committee. It would also mean that those who could not be there in person would be able to see it and give feedback, though this should not discourage people from attending, see point 3, and their feedback has to be given by the time the vetting process ends.

	- The vetting meeting should be started, in the Norway Room, after the Committee Meeting before the publication is due. This ensures that there is enough time to vet properly and for the JdeB to be printed and emailed.

	- Attendance is a serious part of the role of welfare subcommittee. All members should attend, and absentees must have a legitimate apology. At least one member of each committee position should be there for the entirety of the vetting process, unless in exceptional circumstances which must be discussed within welfare subcommittee prior to vetting.

	- Evidence of consent to publish should be obtained, and although it is not the responsibility of welfare subcommittee to receive consent, welfare subcommittee reserve the right to check consent has been given and ask questions about the content of the JdeB. If a person does not respond to a message or email then this does not count as consent, JdeBs can send another request to check if the original message was just not seen, consent has to be given in the form of a response. This consent should be asked for through individual messages or blind CCing in an email.

	- The role of welfare subcommittee in the vetting process is to look over the JdeB and remove anything that does not have permission, targets marginalised groups or anything else that they deem inappropriate.

	- It should be noted that one committee member may have knowledge of a sensitive situation and therefore even if only one committee member wants something removed then it is removed. If no agreement is reached, it will be taken out.

	- The welfare subcommittee has the final say in what can be in the JdeB, unchallenged by the Johns de Balliol.

## Freshers’ Week Subcommittee

- Freshers’ Week Subcommittee is an advisory body which exists to:

	- aid the Vice-President (Organisational) in organising Freshers’ Week;

	- review the preceding Freshers’ Week, and seek improvements, and

	- ensure that there is adequate provision for Freshers’ during the week, paying particular attention to their social welfare and entertainment.

- Freshers’ Week Subcommittee shall consist of:

	- the Vice-President (Organisational);

	- the Entz Officer;

	- the DRs WHO;

	- the Women’s Officers;

	- the Class and First-Generation Officers;

	- the LGBTQ+ Officers;

	- the Ethnic Minorities Officers;

	- the Access and Admissions Officers;

	- the Head Peer Supporter (should such a position exist amongst the peer supporters within the JCR);

	- the Lindsay;

	- the Bar Social Secretary, and;

	- any number of Ordinary Members with an interest in organising Freshers’ Week.

- Freshers’ Week Subcommittee shall operate under the following procedure:

	- The committee shall be chaired and convened by the Vice-President (Organisational), the dates and times of which shall be at their discretion;

	- Meetings of the committee shall be minuted by the Secretary;

	- All members of the committee shall work under the direction of the Vice- President (Organisational);

	- The Vice-President (Organisational) shall present reports to the JCR Committee as and when required by the JCR Committee, and shall present a report on the upcoming Freshers’ Week to the final Ordinary General Meeting of Trinity Term.

- The following Officers shall be responsible for the provision of events during Freshers’ Week:

	- the Vice-President (Organisational) shall be responsible for all administrative and information events during Freshers’ Week;

	- the DRs WHO and the Entz Officers shall be responsible for the provision of daytime and non-alcoholic entertainment during Freshers’ Week and:

		- the Head Peer Supporter shall assist, and

		- the Junior Deans shall assist, should they be willing

	- the Entz Officers shall be responsible for the provision of club nights, as well as entertainment not falling under 4.3b

	- the Lindsay and the Bar Social Secretary shall be responsible for the provision of bar- related entertainment during Freshers' Week; and

	- reasonable provision shall be made by the Freshers’ Week Subcommittee for freshers who do not drink alcohol.

- The following Officers shall be responsible for workshops for Freshers:

	- the DRs WHO and Women’s Officers shall be responsible for the provision of a Consent Workshop at the start of Freshers’ Week, and the training of facilitators for said workshop;

	- the Ethnic Minorities Officers shall be responsible for the provision of a Race Workshop during Freshers’ Week, and the training of facilitators for said workshop

	- the LGBTQ+ Officers shall be responsible for the provision of an LGBTQ+ workshop during Freshers’ Week, and training of facilitators for said workshop

	- the Class and First-Generation Officers shall be responsible for the provision of a Difference Education Workshop during Freshers’ Week, and training of facilitators for said workshop.

## Arts Subcommittee

- The Arts Subcommittee is a body which exists to co-ordinate Arts provision in Balliol, facilitate Arts Week and patronise students across the University.

- The Arts Subcommittee shall consist of:

	- The Treasurer;

	- Maestro Music;

	- the Picture Fund Officer;

	- Comrade Shakespeare;

	- the Drama President;

	- the Drama Treasurer;

	- the Drama Secretary;

	- the Arts Officer;

	- the Pilch Studio Manager;

	- the Pilch Design Officer;

	- the Pilch Marketing Officer.

- Meetings of the Arts Subcommittee shall be chaired by the Arts Officer and minuted by the treasurer.

- The Drama President shall submit a report on their activities to each meeting of the Arts Subcommittee.

## Entz Subcommittee

- The Entz Subcommittee is a body which exists to:

	- assist the Entz Officers in the management of JCR entertainments, and;

	- allocate funds from the Entz Account to societies who submit an application, with the proviso that:

		- no more than £400 shall be spent on any project if money is expected to be taken during the project, and;

		- no more than £200 shall be spent if no money is expected to be taken during the project

- The Entz Subcommittee shall consist of:

	- The Entz Officer;

	- The Lindsay; and

	- Up to five Entz Subcommittee members, elected in accordance with part VIII of the Constitution.

- The Entz Subcommittee shall be chaired and convened by the Entz Officer, who shall submit a report on the activities and finance of the Entz Subcommittee and proposals for forthcoming events to the final Ordinary General Meeting of each term.

- Should a member of the Entz Subcommittee also be a beneficiary of a potential grant, they shall lose their voting rights.

## Standing Subcommittee

- The Standing Subcommittee is a body which exists to consider staff grievances by means of an
interview with the people concerned
- The Standing Subcommittee consists of:

	- The Foody;

	- The President; and

	- The Treasurer.

- The Standing Subcommittee shall be chaired and convened by the Foody.

## Health and Safety Subcommittee

- The Health and Safety Subcommittee is a body which exists to:

	- develop JCR Health and Safety policy, and;

	- liaise with college over matters of risk assessment and safety inspection within the JCR.

- The Health and Safety Subcommittee shall consist of:

	- The Vice-President;

	- The Entz Officer;

	- The Lindsay;

	- The Foody;

	- The Design and Maintenance Officer;

	- The JCR Steward; and

	- The Pilch Studio Manager.

- The subcommittee shall be chaired by the Vice-President and minuted by the Design and Maintenance Officer.

- The subcommittee shall meet at least once a term before College Health and Safety at Work Committee and assign a delegate(s) to attend it.

## Awards Subcommittee

- The Awards Subcommittee is a body which exists to select two recipients of the ‘JCR Teaching Excellence Award’ annually from those members of the University responsible for teaching Balliol JCR members during the previous year.

- The Awards Subcommittee shall consist of:

	- The President;

	- The Secretary;

	- The Academic Affairs Officer; and

	- four Ordinary Members, chosen by the Academic Affairs Officer and the President.

- Nominations shall be open for at least one week, beginning no later than the penultimate Saturday of Hilary Term.

- No nominations may be made by members of the Awards Committee.

- Selection of winners shall be made only on the basis of nominations received, and should be made principally on the quality rather than the quantity thereof.

- The Secretary shall be responsible for the collection of nominations and publicising the awards.

- The Awards Subcommittee shall give notice of their decision no later than the second week of Trinity Term.

## Ball Subcommittee

- The Ball Subcommittee has specific control over the management and execution of the Ball, and shall be granted no additional powers.

- The Ball Subcommittee will consist of:

	- the Ball President;

	- the Ball Treasurer;

	- the Ball Secretary;

	- the Ball Publicity Officer;

	- the JCR Ball Representatives; and

	- the MCR Ball Representatives.

- Ball Subcommittee shall operate under the following procedure:

	- the Ball President shall Chair the meetings;

	- the committee shall meet once a week, at times to be determined by the Ball Secretary; and

	- the Ball Secretary shall minute all meetings of the Ball Subcommittee

- If any of the Offices outlined in clause 9.3 are unfilled at the beginning of the term in which the Ball is to be held, the Ball Subcommittee will have the power to co-opt Ordinary Members to fill these vacant posts.

	- Co-option onto the Ball Subcommittee shall be done in a manner equivalent to co-option onto the Committee, as outlined in part VIII.

- The Ball Subcommittee may not request any loans from College on behalf of the JCR without the approval of the General Meeting.

- It shall be the responsibility of the Ball Subcommittee to present a detailed plan of the event at an Ordinary General Meeting no later than 4th week of the term before the Ball is due to be held.

- If any of the positions of Ball President, Secretary or Treasurer are unfilled at the start of the term in which the Ball is to be held, then the Ball shall be cancelled.

## Pantry Subcommittee

- The PSC is a body which exists to assist and advise the Foodies in the management of Pantry during times in which there are no permanent staff assigned to lunchtime or dinner shifts.

- The PSC shall consist of:

	- the Foodies; and

	- up to five additional PSC Members, elected in accordance with part VIII of the Constitution.

- The Pantry Subcommittee shall be chaired and convened by the Foodies.

# NON-COMMITTEE OFFICES

## List of Non-Committee Offices

- The following positions are Non-Committee Offices:

	- the Duckworth;

	- the Drama President;

	- the Drama Treasurer;

	- the Drama Secretary;

	- the Pilch Studio Manager;

	- the Pilch Design Manager;

	- the Pilch Marketing Officer;

	- Maestro Music;

	- Comrade Tortoise;

	- the Picture Fund Rep;

	- Comrade Shakespeare;

	- Comrade Bike Rep;

	- the Bar Social Secretary;

	- the Ball President;

	- the Ball Treasurer;

	- the Ball Secretary;

	- the Ball Publicity Officer;

	- the Ball Representatives;

	- the Garden Party President;

	- the Garden Party Treasurer;

	- the Garden Party Secretary;

	- the Garden Party Publicity Officer;

	- the Garden Party Representatives;

	- PSC Members;

	- the Arts Officer;

	- Balliol Student Scholarship Representative;

	- the Drama Racial Diversity and Equalities Officer; and

	- the Homelessness Officer

- The Offices of Drama Treasurer, Maestro Music, Comrade Bike Rep, Ball President, Ball Treasurer, Ball Secretary, Ball Publicity Officer, Arts Officer and Balliol Student Scholarship Representative may only be held by a single individual; all other Offices may be held by a single individual or by dual nominations.

- The duties of the Non-Committee Offices are given in the following sections; these duties are binding upon the Members holding the Non-Committee Offices.

## Duties of the Duckworth

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Duckworth shall assist the Lindsay in the execution of their duties, and shall act under their direction.

## Duties of the Drama President

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Drama President shall:

	- promote and support dramatic productions and events involving Members;

	- award loans to dramatic productions, provided that:

		- at least one member of the production crew or cast is an Ordinary Member; and

		- at the time of making the loan, the production has a confirmed director and producer, and either has booked or is applying to book a venue; and

		- the contract for any loan should have the form given in part VI of these Standing Orders; and

		- decision of what action should be taken in the case of breach of contract is at the discretion of the Committee; and

		- the Drama Treasurer and Drama Secretary are in agreement on the matter (subject to the occupancy of those positions).

	- ensure that the Treasurer is fully informed of the nature, details and status of any loans; and

	- submit a report to each meeting of the Arts Subcommittee detailing their activities; and

	- work with the Pilch Studio Manager to ensure that the Michael Pilch Studio is managed effectively, conducting activities including but not exclusive to:

		- agree with the Balliol College Dean the available show dates for the following term;

        - accept and study bid applications from production companies throughout Oxford, not limited to Balliol College, no later than Saturday 6th Week;

	    - interview each production company which has submitted a bid;

		- schedule (subject to the Dean’s agreement, in accordance with part i) successful bids in the available weeks;

		- keep the Dean, the Head Porter, the Jowett Wardens, and the Steward of the Pilch apprised of the confirmed termcard and any subsequent changes made to it;

		- ensure that the Balliol College Bursary is provided with the necessary information to invoice visiting companies;

		- keep Sundays and Mondays at the Pilch available for use by any Balliol member by booking with the Porters Lodge,

		- ensure that all production companies sign “The Pilch Contract”;

		- Ensure that a general production meeting be shall held no later than Saturday of 0th week of each term, consisting of at least the President and Studio Manager, as well as representatives from all shows scheduled commencing term, at which all the necessary safety and security protocols as outlined in the Pilch Contract are clearly communicated to the production companies;

		- agree with the Drama President who will be on call for each show’s “get-in” and “get-out”, which involves briefly visit the “get-in” and “get-out” of each show to ensure that the crew is following correct health and safety procedures, and being immediately contactable during the duration of the “get-in” and “get-out”, in case the crew needs assistance. The representative on call should be either the Drama President or the Pilch Studio Manager; should neither of these individuals be available, another member of the Drama Committee should be allocated the role on a show-by-show basis;

		- the Society must ensure that all productions fulfil their contractual obligations, as outlined in “The Pilch Contract”;

		- ensure that the Dean, the Domestic Bursar, and the Steward of the Pilch are kept abreast of any changes to the Pilch contract;

		- ensure the society and the Pilch Studio are governed in accordance with the Society’s own Drama Constitution.

- The Drama President is to be responsible for running the yearly election of the Balliol College Drama Society Committee.

	- The BCDS elections are separate to the JCR Committee elections;

	- only members of the outgoing BCDS Committee are eligible to vote;

	- the elections are to take place in late Michaelmas term or early Hilary term; and

	- if any dispute arises regarding the election process that cannot be resolved by the outgoing BCDS Committee, it is to be brought to a JCR GM for resolution.

	- The BCDS Committee consists of:
		- The Drama President;
		- the Drama Treasurer;
		- the Drama Secretary;
		- the Pilch Studio Manager;
		- the Pilch Design Officer; and
		- the Pilch Marketing Officer.

## Duties of the Drama Treasurer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Drama Society Treasurer shall:

	- assist the Drama President in assessing the viability of loans to dramatic productions; and

	- assist the Drama President and the Treasurer in the production of reports and statements of accounts related to loans to dramatic productions;

## Duties of the Drama Secretary

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Drama Society Secretary shall assist the Drama President with the production of reports and the keeping of records related to loans to dramatic productions.

## Duties of the Pilch Studio Manager

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Pilch Studio Manager shall:

	- agree with the Drama President who will be on call for each show’s “get-in” and “get-out”, which involves briefly visit the “get-in” and “get-out” of each show to ensure that the crew is following correct health and safety procedures, and being immediately contactable during the duration of the “get-in” and “get-out”, in case the crew needs assistance. The representative on call should be either the Drama President or the Pilch Studio Manager; should neither of these individuals be available, another member of the Drama Committee should be allocated the role on a show-by-show basis;

	- attend all bid interviews for prospective shows;

	- attend the General Production Meeting each term, to ensure that relevant crew members receive appropriate health and safety training, and;

	- agree with the Drama President and the Drama Treasurer what maintenance should be conducted at the end of each term, and ensure that this maintenance is carried out before the General Production Meeting at the start of the following term; and

	- keep an up-to-date inventory of all the equipment housed in the Pilch.

## Duties of the Pilch Design Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Pilch Design Officer shall:

	- attend the termly bid interviews with a view to assessing the design elements of the bids (including but not limited to the lighting, set, costume and sound designs); whether they form a coherent vision; whether they are practically viable; and whether they are in line with the “Philosophy of the Pilch” as outlined in Section 15 of the Drama Society Constitution;

	- advise visiting productions on the design potentials and limitations of the Pilch.

## Duties of the Pilch Marketing Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Pilch Marketing Officer shall:

	- ensure that the Pilch Website (www.pilchstudio.com) is up-to-date;

	- ensure that visiting Productions are supported by an active Pilch Social Media presence;

	- ensure that the Pilch maintains a vital presence in Oxford University according to the “Philosophy of the Pilch” as outlined in Section 15 of the Drama Society Constitution.

## Duties of Maestro Music

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, Maestro Music shall be responsible for the promotion of musical activity within the JCR.

- A compulsory levy shall be used to support a Music Fund, to be administered by Maestro Music in accordance with part IV, section 5 of the Constitution to be devoted to the promotion of musical activities within the JCR.

## Duties of Entz Subcommittee Officers

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the five Entz Subcommittee members shall:

	- sit on Entz Subcommittee;

	- assist the Entz Officers in the execution of their duties; and

	- act under their direction.

## Duties of Comrade Tortoise

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee Comrade Tortoise shall:

	- care for, as far as is possible, Rosa Luxembourg, the JCR tortoise; and

	- train the said tortoise for its big race against the Corpus Tortoise in Trinity Term.

## Duties of The Picture Fund Rep

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Picture Fund Rep shall administer the Picture Fund as laid down in the Standing Orders.

- A Picture Fund, to be administered by the Picture Fund Rep in accordance with part IV, section 5 of the Constitution, shall be devoted to the buying, up-keep and selling of works of art for display in the JCR and for letting to members resident in College.

- To purchase a work costing more than £75, or to sell any work of art belonging to the fund, the Picture Fund Rep requires the consent of the Committee, with the right of recourse to a GM should the Committee withhold consent.

## Duties of Comrade Shakespeare

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee Comrade Shakespeare shall

	- co-ordinate the editing of Scrawl magazine;

	- be responsible for the promotion of new writing in Balliol; and

	- organise events pertaining to their interests in the Arts Festival.

## Duties of Comrade Bike Rep

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee Comrade Bike Rep shall:

	- provide information about bicycle safety to JCR members;

	- provide information about bicycle maintenance to JCR members; and

	- be responsible for any other bike-related matters.

## Duties of Bar Social Secretary

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee The Bar Social Secretary shall:

	- organise and promote regular entertainment in the Lindsay Bar including but not limited to quiz nights, karaoke and open mic nights;

	- work closely with the Lindsay and the Duckworth, who will provide any and all needed technical and logistical support.

## Duties of the Ball President

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Ball President shall:

	- take overall control of the Ball, producing deadlines for various aspects of its completion and ensuring that they are adhered to;

	- chair all meetings of the Ball Subcommittee;

	- present reports regarding the Ball to the JCR Committee as and when required by the JCR Committee; and

	- present a final report and accounts at the final Ordinary General Meeting of the term during which the Ball was held. If there are no subsequent Ordinary General Meetings after the Ball, the Ball President shall present this report to the General Meeting at the earliest possible opportunity.

## Duties of the Ball Treasurer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Ball Treasurer shall:

	- be responsible for the accounts of the Annual Ball; and

	- present a budget to the JCR Treasurer no less than 30 days before the Annual Ball.

## Duties of the Ball Secretary

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Ball Secretary shall:

	- be responsible for organising weekly meetings of the Ball Subcommittee;

	- minute said meetings; and

	- ensure that all paperwork pertaining to the Annual Ball is in order.

## Duties of the Ball Publicity Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Ball Publicity Officer shall:

	- take overall control of publicity and marketing for the Ball; and

	- seek sponsorship for the event.

## Duties of the Ball Representatives

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Ball Representatives shall:

	- sit on the Ball Subcommittee; and

	- work under the supervision of the Ball President.

## Duties of the Garden Party President

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Garden Party President shall:

	- take overall control of the Garden Party, producing deadlines for various aspects of its completion and ensuring that they are adhered to;

	- chair all meetings of the Garden Party Subcommittee;

	- present reports regarding the Garden Party to the JCR Committee as and when required by the JCR Committee; and

	- present a final report and accounts at the final Ordinary General Meeting of the term during which the Garden Party was held. If there are no subsequent Ordinary General Meetings after the Garden Party, the Garden Party President shall present this report to the General Meeting at the earliest possible opportunity.

## Duties of the Garden Party Treasurer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Garden Party Treasurer shall:

	- be responsible for the accounts of the Garden Party

	- present a budget to the JCR Treasurer no less than 30 days before the Annual Ball.

## Duties of the Garden Party Secretary

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Garden Party Secretary shall:

	- be responsible for organising weekly meetings of the Garden Party Subcommittee;

	- minute said meetings; and

	- ensure that all paperwork pertaining to the Garden Party is in order.

## Duties of the Garden Party Publicity Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Garden Party Publicity Officer shall:

	- take overall control of publicity and marketing for the Garden Party; and

	- seek sponsorship for the event.

## Duties of the Garden Party Representatives

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Representatives shall:

	- sit on the Garden Party Subcommittee; and

	- work under the supervision of the Garden Party President.

## Duties of PSC Members

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee, each Pantry Subcommittee (PSC) Member shall:

	- Carry out a minimum of two food-preparation shifts (also known as a “back shift”) per week during periods when PSC is activated;

	- in the case that one of the Foodies cannot fulfil the entirety of their role due to incapacity, removal or resignation, one of the PSC members may take on the role in an acting capacity in the interim.

## Duties of Johns de Balliol

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the John de Balliol shall

	- produce and distribute ‘John de Balliol’, which shall be the JCR gossip and entertainment publication; and

	- write and direct the JCR Christmas pantomime.

- The John de Balliol shall take part in the vetting process agreed between the welfare subcommittee and John de Balliol as detailed under the duties of welfare subcommittee with the following differences:

	- The Officers will send out an email 1 week before each bop explaining:
		1. that people will be included;
		2. that they can ask for more information, say no, or give a blanket consent;
		3. that welfare can be contacted if they do not feel comfortable with J de B’s;
		4. that nothing will go in without their express permission;
		5. that submissions are required;
		6. that J de B prepare a publication including only stories that have received consent; and
		7. that vetting will occur as per usual.

	- Vetting will take place in JCR office, and the Officers come to introduce and present the publication. The Officers are not to be present during the vetting process and have no opportunity to question a decision: they are simply to be told that something has been removed and that it cannot be included.

	- The publication will be displayed for the entirety of the bop, and then removed at earliest possible convenience. This is the responsibility of the Officers.

## Duties of the Arts Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Arts officer shall:

	- chair arts subcommittee at least twice per term;

	- lead the organisation of arts week in 5th week Michaelmas;

	- promote the engagement with and organisation of arts related activities in Balliol beyond the scope of arts week;

	- encourage the coordination of members of arts subcommittee;

## Duties of the Balliol Student Scholarship Representative

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Student Scholarship Rep shall:

	- increase awareness of this scholarship;

	- support all potential and ongoing applications; and

	- work with the College Office and Development Office to ensure the best possible running of the scholarship.

## Duties of the Drama Racial Diversity and Equalities Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Drama Racial Diversity and Equalities Officer shall:

	- assist the rest of committee in the selection of term cards for the Pilch, ensuring that the term card reflects a wide diversity of talent

- This position can be held by two people. In the event of the position being held by two individuals, at least one holder of this role should be held by a member of the BAME community.

## Duties of the Homelessness Officer

- In addition to other duties imposed by the Constitution and Standing Orders and by the resolutions of the Committee or of General Meetings, the Homelessness Officer shall:

	- twice termly publicise and collect food, sanitary items and other donations for TSHA and/ or other homelessness organisations at their discretion;

	- publicise meetings and volunteering opportunities for homelessness work;

	- work with the ‘On Your Doorstep’ campaign (OYD) and the JCR to empower and help homeless people in other suitable ways;

	- work with OYD and the JCR to consistently and long-termedly pressure college to empower and aid homeless people (for example, through donating unused uncooked produce from Hall as is done at several other colleges); and

	- sign up as one of OYD’s informal reps at Balliol to enable (a), (b), (c) and (d).
